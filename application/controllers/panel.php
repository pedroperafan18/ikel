<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->library(array('session','form_validation'));
        $this->load->model('madmin');
        /*
        $this->load->model('minicio');
        */
    }

    public function index() {
        $data_head["titulo"]="Bienvenido | IKEL";
        $data_head["descripcion"]="Todos tienen algo que aportar.";
        $data_head["keywords"]="";
        $data_head["ico"]=base_url("assets/img/ico.png");
        $data_head["css"]=array("materialize.min","font-awesome.min","paladium.min","jquery.dataTables");
        $data_javascript["script"]=array("jquery","materialize.min","general.min","jquery.dataTables","zjs.utils","jQuery.dtplugin","admin/admin");
        // Fin codigo index

        // Inicio vista index
        $data["head"]       = $this->load->view("head",$data_head,TRUE);  
        $data["javascript"] = $this->load->view("javascript",$data_javascript,TRUE);
        $data["menu"]       = $this->load->view("menu","",TRUE);
        //$data["footer"]     = $this->load->view("footer","",TRUE);
        $data["contenido"]  = $this->load->view("admin/rdcursos",NULL,TRUE);
        $this->load->view("html",$data);
        // Fin vista index.
    }

    public function getCursosDatatable()
    {
        if($this->input->is_ajax_request()){
            if($this->input->post()!=NULL){
                $this->dataTable("cursos cu");
            }
        }
    }
//Fin metodo getvideos
    public function dataTable($tabla) {  
        $this -> load -> library('Datatable', array('model' => 'mpaginacion', 'rowIdCol' => 'ID'));
        $jsonArray = $this -> datatable -> datatableJson(
            array(
                'tabla' => $tabla
            )
        );
        $this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));
    }

}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */ 