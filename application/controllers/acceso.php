<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acceso extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('session','form_validation','user_agent'));
        $this->load->helper('url', 'security');
        $this->load->model('macceso');
        $this->CI = & get_instance();
	}

	public function index()
	{
	 	if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
	 		$perfil = $this->session->userdata('perfil');
        	if ($perfil == 1) {
            	$perfil = "admin";
                redirect($perfil,"refresh");
            }elseif ($perfil == 2) {
            	$perfil = "maestro";
                redirect($perfil,"refresh");
            }elseif ($perfil == 3) {
            	$perfil = "usuario";
                redirect($perfil,"refresh");
            }elseif ($perfil == 4) {
                $this->eleccion();
            }
        }else{
            redirect("acceso/login","refresh");
        }
	}

	function logData(){
        
        date_default_timezone_set("America/Mexico_City");

        $times = $this->CI->db->query_times; 
        foreach ($this->CI->db->queries as $key => $query) { 
            if (!preg_match('/SELECT/', $query)) {
                $query = str_replace("INTO ", "", $query);
                $sql = explode(' ',$query);
                $data["IDUsuario"] = $this->session->userdata('identificador');
                $data["Accion"] = $sql[0];
                $data["Tabla"] = str_replace("`", "", $sql[1]);
                $data["IP"] = $this->input->ip_address();
                $patron = "/([0-9]+)/";
                if (preg_match($patron, $query,$id)) {
                    $data["IDElemento"] = $id[1];
                }else{
                    $data["IDElemento"] = 0;
                }                
                if ($this->agent->is_browser())
                {
                    $data["Navegador"] = $this->agent->browser().' '.$this->agent->version();
                }
                elseif ($this->agent->is_robot())
                {
                    $data["Navegador"] = $this->agent->robot();
                }
                elseif ($this->agent->is_mobile())
                {
                    $data["Navegador"] = $this->agent->mobile();
                }
                else
                {
                    $data["Navegador"] = 'OS sin identifiar';
                }
                $data["SistemaOperativo"] = $this->agent->platform();
                $data['Fecha'] = date('Y-m-d H:i:s');
                $this->macceso->setLog($data);
            }
                
        }

    }

	public function login(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            print_r("expression");
        	$perfil = $this->session->userdata('perfil');
        	if ($perfil == 1) {
            	$perfil = "Admin";
            }elseif ($perfil == 2 || $perfil == 4) {
            	$perfil = "Maestro";
            }elseif ($perfil == 3) {
            	$perfil = "Usuario";
            }
            redirect($perfil."/","refresh");
        }else{
            if($this->input->is_ajax_request()!=FALSE){
                $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
                if($this->input->post()!=NULL){
                	foreach ($data as $key => $value) {
	                    $data["$key"] = $this->security->xss_clean($value);
	                }
                    $this->form_validation->set_rules('Correo', 'Correo electrónico', 'trim|required|valid_email');
                    $this->form_validation->set_rules('Password', 'Contraseña', 'trim|required|max_length[16]');
                    if ($this->form_validation->run() == FALSE) {
                        $errors = $this->form_validation->error_array();
                   		$array = array("result" => FALSE,"error" => $errors);
                    }else {
                        $data["Password"] = sha1(md5($data["Password"]));
                        $result = $this->macceso->login($data);
                        if($result!=FALSE){
                            $id = $result["0"]["ID"];
                            $perfil = $result["0"]["Tipo"];
                            $array = array(
                                'identificador' => $id,
                                'perfil'    => $perfil
                            );
                            $this->session->set_userdata($array);
                            $array = array('result' => TRUE);
                        }else{
                        	$array = array('result' => FALSE, 'error' => 'Usuario / Contraseña incorrecto(s)');
                        }  
                    }
                }
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }else{
               
                $data_header["titulo"] = "nombre";
                $data_header["descripcion"] = "";
                $data_header["imagen"] = "";
                $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
                $data_header["cssExterno"] = array();
                $data_javascript["script"] =array("jquery","materialize.min","general.min","inicio/acceso");
                $data_javascript["scriptExterno"] = array();
                $data_extras["contenido"] = ""; //Puede ser un array
                $data["contenido"] = $this->load->view("acceso/login", NULL , TRUE);
                $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
                $data["head"] = $this->load->view("head", $data_header, TRUE);
                $data["menu"] = $this->load->view("menu", NULL , TRUE);
                $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
                $data["footer"] = $this->load->view("footer", NULL , TRUE);
                $this->load->view("html",$data);
                
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('identificador');
        $this->session->unset_userdata('perfil');
        redirect('inicio','refresh');
    }

    public function registro(){
    	if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
        	$perfil = $this->session->userdata('perfil');
        	if ($perfil == 1) {
            	$perfil = "Admin";
            }elseif ($perfil == 2 || $perfil == 4) {
            	$perfil = "Maestro";
            }elseif ($perfil == 3) {
            	$perfil = "Usuario";
            }
            redirect($perfil."/","refresh");
        }else{
	        if($this->input->is_ajax_request()){
	            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
	            if($this->input->post()!=NULL){
	            	foreach ($data as $key => $value) {
	                    $data["$key"] = $this->security->xss_clean($value);
	                }
	                
	                $this->form_validation->set_rules('Nombres','Nombre completo','trim|required|min_length[4]|max_length[150]');
	                $this->form_validation->set_rules('ApellidoPaterno','Apellido Paterno','trim|required|min_length[4]|max_length[150]');
	                $this->form_validation->set_rules('ApellidoMaterno','Apellido Materno','trim|min_length[4]|max_length[150]');
	                $this->form_validation->set_rules('Correo', 'Correo', 'trim|required|is_unique[usuarios.Correo]');
	                $this->form_validation->set_rules('Password','Contraseña','trim|required|min_length[8]|max_length[16]');
	                if ($this->form_validation->run() != FALSE) {
	                    $data["Password"] = sha1(md5($data["Password"]));
                        $data["Tipo"] = 3;
	                    $result = $this->macceso->nuevousuario($data);

	                    if ($result != FALSE) {
                            $id = $result;
                            $perfil = 3;
                            $array = array(
                                'identificador' => $id,
                                'perfil'    => $perfil
                            );
                            $this->session->set_userdata($array);
	                        $array = array("result" => TRUE);
                            $this->logData();
	                    }else{
	                        $array = array("result" => FALSE,"error" => "Error al insertar los datos, vuelve a intentarlo");
	                    }
	                } else {
	                    $errors = $this->form_validation->error_array();
	                    $array = array("result" => FALSE,"error" => $errors);
	                }
	                $this->output->set_content_type("application/json")->set_output(json_encode($array));
	            }
	        }
	    }
    }

    public function gustos(){
    	if(!$this->session->userdata('identificador') || !$this->session->userdata('perfil')){
        	redirect("acceso/login","refresh");
        }else{
	        if($this->input->is_ajax_request()){
	            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
	            if($this->input->post()!=NULL){
	            	foreach ($data as $key => $value) {
	                    $data["$key"] = $this->security->xss_clean($value);
	                }
	                $categorias=explode(',',$data['Categorias']);
	                $data = "";
	                $result = TRUE;
	                $data["IDUsuario"] = $this->session->userdata('identificador');
	                $this->macceso->adiosGustos($data["IDUsuario"]);
                    for($i=0;$i<count($categorias);$i++) {
	                	$data["IDCategoria"] = $categorias[$i];
	                    $result = $this->macceso->cargarcaracteristicas($data);
	                    if($result==FALSE)	{
	                    	$result = FALSE;
	                    }
	                }
                    if ($result != FALSE) {
                        $array = array("result" => TRUE);
                    }else{
                        $array = array("result" => FALSE,"error" => "Error al insertar los datos, vuelve a intentarlo");
                    }
	                $this->output->set_content_type("application/json")->set_output(json_encode($array));
	            }
	        }
	    }
    }

    public function eleccion(){
        if($this->input->is_ajax_request()!=FALSE){
            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            if($this->input->post()!=NULL){
                foreach ($data as $key => $value) {
                    $data["$key"] = $this->security->xss_clean($value);
                }
                $id = $this->session->userdata('identificador');
                $perfil = $data['Tipo'];
                $array = array(
                    'identificador' => $id,
                    'perfil'    => $perfil
                );
                if ($this->session->set_userdata($array)) {
                    $array = array('result' => TRUE);
                }
            }
            $this->output->set_content_type("application/json")->set_output(json_encode($array));
        }else{
            $data_header["titulo"]="Bienvenido | IKEL";
            $data_header["descripcion"]="Todos tienen algo que aportar.";
            $data_header["imagen"] = "";
            $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
            $data_header["cssExterno"] = array();
            $data_javascript["script"] =array("jquery","materialize.min","general.min","inicio/acceso");
            $data_javascript["scriptExterno"] = array();
            $data_extras["contenido"] = ""; //Puede ser un array
            $data["contenido"] = $this->load->view("eleccion", NULL , TRUE);
            $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
            $data["head"] = $this->load->view("head", $data_header, TRUE);
            $data["menu"] = $this->load->view("menu", NULL , TRUE);
            $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
            $data["footer"] = $this->load->view("footer", NULL , TRUE);
            $this->load->view("html",$data);
            
        }
    }
}

/* End of file acceso.php */
/* Location: ./application/controllers/acceso.php */ 