$(document).ready(function(){
	$("#EnviarSolicitud").click(function(){
		var Nombre = $("#Nombre").val();
		var ApellidoPaterno = $("#ApellidoPaterno").val();
		var ApellidoMaterno = $("#ApellidoMaterno").val();
		var Prueba = $("#Prueba").val();
		var Correo = $("#Correo").val();
		var Categoria = $("#Categoria").val();
		console.log(Categoria);
		$.ajax({
			url: base_url+'maestro/solicitud',
			type: 'POST',
			data: {
				Nombre:Nombre,
				ApellidoPaterno:ApellidoPaterno,
				ApellidoMaterno:ApellidoMaterno,
				Prueba:Prueba,
				Correo:Correo,
				Categoria:Categoria
			},
		})
		.done(function(data) {
			console.log(data);
			if(data.result == true){
				$("#Nombre").val(" ");
				$("#ApellidoPaterno").val(" ");
				$("#ApellidoMaterno").val(" ");
				$("#Prueba").val(" ");
				$("#Correo").val(" ");
				Materialize.toast("Solicitud enviada", 3000, 'rounded');
			}
		})
		.fail(function(data) {
			if(typeof data.responseText != "undefined"){
				console.log(data.responseText);
			}
		});
	});
	
	
});