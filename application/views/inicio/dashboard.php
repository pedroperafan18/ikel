<div class="container">
	
	<?php
	if(is_array($solicitudes)){
		echo '<h4>Posibles proximos cursos</h4>
	<p>Ve los siguientes videos y dinos si quisieras ver los cursos completos.</p>';
		$numero = count($solicitudes);
		foreach ($solicitudes as $key => $video) {
			if($key%2 == 0){
				echo '<div class="row">';
				}
			?>
			<div class="col s6">
				<div class="card">
					<div class="card-image">
						<div class="video-container">
							<iframe width="100%" height="450" src="https://www.youtube.com/embed/<?=$video["Prueba"]?>" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="card-content">
						<span class="card-title"><?=$video["NombreCurso"]?></span>
					</div>
					<div class="card-action">
		              <a href="#">Quiero ver este curso completo</a>
		            </div>
				</div>
			</div>
			<?php
			if($numero%2 != 0){
				if(($key+1)==$numero ){
					echo '</div>';
				}
			}else{
				if($key%2 != 0){
					echo '</div>';
				}
			}
				
		}
	}
	?>
	<h4>Cursos que te van a interesar</h4>
	<?php
	if(is_array($mascursos)){
		$numero = count($mascursos);
		foreach ($mascursos as $key => $val) {
			if($key%2 == 0){
				echo '<div class="row">';
				}
			?>
			<div class="col s6">
				<div class="card">
					
					<div class="card-content">
						<span class="card-title"><?=$val["NombreCurso"]?></span>
					</div>
					<div class="card-action">
		              <a href="<?php echo  base_url("inicio/curso/".$val["ID"]);?>">Ver curso completo</a>
		            </div>
				</div>
			</div>
			<?php
			if($numero%2 != 0){
				if(($key+1)==$numero ){
					echo '</div>';
				}
			}else{
				if($key%2 != 0){
					echo '</div>';
				}
			}
		}
	}else{
		echo "<h3>Upps, no encontramos ningun curso que te pueda interesar.</h3>";
		echo "<h4>Para definir nuevamente tus gustos puedes ir a <a href='".base_url("usuario/gustos")."'>gustos</a></h4>";
	}
	?>
</div>
