$(document).ready(function(){  

	$("#registro").click(function(){
    	var nombre = $("#Nombre").val();
		var materno = $("#Materno").val();
		var paterno = $("#Paterno").val();
		var correo = $("#Correo").val();
		var pass = $("#Password").val();

		$.ajax({
			url: base_url+'acceso/registro',
			type: 'POST',
			data: {Nombre:nombre,ApellidoPaterno:paterno,ApellidoMaterno:materno,Correo:correo,Password:pass},
		}).always(function(data) {
			if(data.result!=false){
				window.location=base_url+"usuario";
			}else{
				$.each(data.error, function(index, val) {
					Materialize.toast(val, 12000, 'rounded');
				});
			}
			console.log("complete");
		});
	});

	$("#ingreso").click(function(){
    	var correo = $("#correo").val();
		var pass = $("#password").val();
		
		$.ajax({
			url: base_url+'acceso/login',
			type: 'POST',
			data: {Correo:correo,Password:pass},
		}).always(function(data) {
			console.log(data);
			if(data.result!=false){
				window.location=base_url+"usuario";
			}else{
				if(typeof data.error == "array"){
					$.each(data.error, function(index, val) {
						if (typeof val != 'undefined') {
							Materialize.toast(val, 12000, 'rounded');
						}
					});	
				}else{
					terialize.toast(data.error, 12000, 'rounded');
				}
				
			}
			console.log("complete");
		});
	});

	$("#gustos").click(function(){
		var categorias = new Array();
		var fd = new FormData();

		$("input[type=checkbox]:checked").each(function(){
			categorias.push($(this).val());
		});    

		fd.append('Categorias',categorias);

		$.ajax({
	        url: base_url+"acceso/gustos",
	        type:"POST",
	        contentType: false,
	        data: fd,
	        processData: false,
	        cache: false,
		}).always(function(data) {
			if(data.result!=false){
				window.location=base_url+"usuario";
			}else{
				$.each(data.error, function(index, val) {
					Materialize.toast(val, 12000, 'rounded');
				});
			}
			console.log("complete"); 
		});
	});

	$("#alumno").click(function(){
		$tipo=3;
		$.ajax({
			url: base_url+'acceso/tipo',
			type: 'POST',
			data: {Tipo:tipo},
		}).always(function(data) {
			if(data.result!=false){
				window.location=base_url+"usuario";
			}else{
				$.each(data.error, function(index, val) {
					Materialize.toast(val, 12000, 'rounded');
				});
			}
			console.log("complete");
		});
	});
	$("#maestro").click(function(){
		$tipo=2;
		$.ajax({
			url: base_url+'acceso/tipo',
			type: 'POST',
			data: {Tipo:tipo},
		}).always(function(data) {
			if(data.result!=false){
				window.location=base_url+"maestro";
			}else{
				$.each(data.error, function(index, val) {
					Materialize.toast(val, 12000, 'rounded');
				});
			}
			console.log("complete");
		});
	});
});