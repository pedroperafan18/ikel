
<div class="wrapper">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="row table-wrapper-row">
                <div class="col s12 m12 l12 table-wrapper-col">
                    <table id="cursosTabla" class="responsive-table tabla-dashboard">
                        <thead>
                            <tr>
                                <td><input type="text" placeholder="Búsqueda ID" /></td>
                                <td><input type="text" placeholder="Nombre" /></td>
                                <td><input type="text" placeholder="Fecha Inicial" /></td>
                                <td><input type="text" placeholder="Fecha Final" /></td>
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Fecha Inicial</th>
                                <th>Fecha Final</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>