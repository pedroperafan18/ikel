<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Macceso extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function login($data)
	{
		$this->db->where('Correo', $data["Correo"]);
		$this->db->where('Password', $data["Password"]);
		$query = $this->db->get('usuarios');
		return (($query->num_rows()>0)?$query->result_array():NULL);
	}
	public function nuevousuario($data)
	{
		return (($this->db->insert('usuarios', $data))?$this->db->insert_id():FALSE);
	}
	public function setLog($data)
	{
		$this->db->insert('log',$data);
	}
	public function cargarcaracteristicas($data)	
	{
		return $this->db->insert('gustosusuario', $data);
	}
	public function adiosGustos($id)
	{
		$this->db->where('IDUsuario', $id);
		$this->db->delete('gustosusuario');
	}
}

/* End of file Macceso.php */
/* Location: ./application/models/Macceso.php */