<div class="container">
	<div class="row modal-header-row">
		<div class="col s12 m12 l12 modal-header-col">
			<div class="row">
				<div class="col s12 m12 l4 push-l5">
					<img class="responsive-img" src="<?=base_url('assets/img/inicio/avatar.png')?>">
				</div>
				
			</div>
			<div class="row">
				<div class="col s12 m12 l12">
					<h1>Ingresar</h1>
					<p>Ingresa tus datos para poder acceder</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<form class="col s12 m8 l8 offset-m2 offset-l2">
			<div class="container">
				<div class="input-field col s12 m12 l12">
  					<input id="correo" type="email" class="validate" required>
  					<label for="correo">Correo Electrónico</label>
				</div>
				<div class="input-field col s12 m12 l12">
  					<input id="password" type="password" class="validate" required="">
  					<label for="password">Contraseña</label>
				</div>
				<div class="row">
    				<div class="col s12 m12 l12 submit-ingreso">
        				<button type="button" class="bttn bttn-submit" id="ingreso">Ingresar</button>
    				</div>
				</div>
			</div>
		</form>
	</div>
</div>