<div class="wrapper video-wrapper">
	<div class="row video-row">
		<div class="col s12 m12 l12 video-col">
		<div class="row video-titulo-row">
			<div class="col s12 m12 l8 push-l2 video-titulo-col">
				<h1><?php echo $NombreClase;?></h1>
			</div>
		</div>
		<div class="adaptador">
			<div class="row video-reproductor-row">
				<div class="col s12 m12 l8 push-l2 video-reproductor-col">
					<iframe width="100%" height="450" src="https://www.youtube.com/embed/<?php echo $Contenido;?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>

		<div class="row video-container-row">
			<div class="col s12 m12 l8 push-l2 video-container-col">
				<div class="row video-profesor-row">
					<div class="col s12 m12 l6 video-profesor-nombre">
						<h4><?php echo $Nombres." ".$ApellidoPaterno." ".$ApellidoMaterno;?></h4>
						<h5><?php echo $Fecha;?></h5>
						<!--<p>Categoria</p>-->
					</div>
					<!--
					<div class="col s12 m12 l4 video-profesor-puntuacion">
						<p class="range-field">
		      				<input type="range" id="test5" min="0" max="100" />
		    			</p>
					</div>
					-->
				</div>
				<div class="row video-descripcion-row">
					<div class="col s12 m12 l12 video-descripcion-col">
						<p><?php echo $Descripcion;?></p>
					</div>
				</div>
				<div class="row video-comentario-row">
					<div class="col s12 m12 l12 video-comentario-col">
						<form>
							<textarea id="comenatario" value="" placeholder="Escribe tu comentario..." maxlength="500"></textarea>
							<button type="button" class="btt bttn-submit" id="comentar">Enviar</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>