<div class="container">
	<div class="row">
		<div class="col s6 offset-s3">
			<div class="row">
				<div class="input-field col s12">
					<input placeholder="Nombre" id="Nombre" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input placeholder="Apellido Paterno" id="ApellidoPaterno" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input placeholder="Apellido Materno" id="ApellidoMaterno" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				Sube un video a Youtube, compartenos que es lo que haces y porque quieres compartirlo con el mundo :D
				<div class="input-field col s12">
					<input id="Prueba" type="text" class="validate" placeholder="https://www.youtube.com/watch?v=#######">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input placeholder="Correo" id="Correo" type="email" class="validate">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<select id="Categoria">
						<option value="" disabled selected>Categoria</option>
						<?php
						$categorias = array("0" => array("ID"=> "1","Nombre"=> "Nueva"));
						foreach ($categorias as $key => $categoria) {
							echo '<option value="'.$categoria["ID"].'">'.$categoria["Nombre"].'</option>';
						}
						?>
					</select>
					<label>Categoria</label>
				</div>
			</div>
			<div id="EnviarSolicitud" class="btn btn-primary">Enviar</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
		</div>
	</div>
</div>