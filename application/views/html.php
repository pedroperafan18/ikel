<?php
if(!(isset($head))||!(isset($menu))||!(isset($contenido))||!(isset($javascript))){
	echo "Algun elemento indispensable no fue cargado en el controlador";
	exit;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php echo $head;?>
</head>
<body><!--
	<div class="loader">
        <div class="load">
           	<img src="<?=base_url('assets/img/loader.gif')?>">
        </div>
    </div>
    -->

	<?php
		if(isset($extras))
			echo $extras;
	?>
	<?php echo $menu;?>
	<?php echo $contenido;?>
	<?php
		if(isset($footer))
			echo $footer;
	?>
	<?php echo $javascript;?>
</body>
</html>
