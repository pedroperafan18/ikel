<div class="wrapper video-wrapper">
	<div class="row video-row">
		<div class="col s12 m12 l12 video-col">
		<div class="row video-titulo-row">
			<div class="col s12 m12 l8 push-l2 video-titulo-col">
				<h1><?php if(isset($proyecto)&&!empty($proyecto)) echo $proyecto[0]["Nombre"]; ?></h1>
			</div>
		</div>
		<div class="adaptador">
			<div class="row video-reproductor-row">
				<div class="col s12 m12 l8 push-l2 video-reproductor-col">
					<iframe width="100%" height="450" src="<?php if(isset($proyectoimg)&&!empty($proyectoimg)) echo $proyectoimg[0]['Contenido']; ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>

		<div class="row video-container-row">
			<div class="col s12 m12 l8 push-l2 video-container-col">
				<div class="row video-profesor-row">
				</div>
				<div class="row video-descripcion-row">
					<div class="col s12 m12 l12 video-descripcion-col">
						<p><?php if(isset($proyecto)&&!empty($proyecto)) echo $proyecto[0]["Descripcion"]; ?></p>
					</div>
				</div>
				<div class="row">
				<?php 
				if((isset($comentarios)&&is_array($comentarios))&&(isset($usuarios)&&is_array($usuarios)))
					foreach ($comentarios as $key => $value) {
						echo'<div class="col s12 m12 l12 video-comentario-col">
							<blockquote>'.$usuarios[$key]["Nombres"].' '.$usuarios[$key]["ApellidoPaterno"].'</blockquote>
							<p>'.$value["Comentario"].'</p>
						';
					}
				?>
				</div>
				<div class="row video-comentario-row">
					<div class="col s12 m12 l12 video-comentario-col">
						<form>
							<textarea id="comentario" value="" placeholder="Escribe tu comentario..." maxlength="500">
							</textarea>
							<button type="button" data-id ="<?= $proyecto[0]['ID']?>" class="btt bttn-submit" id="comentar">Enviar</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>