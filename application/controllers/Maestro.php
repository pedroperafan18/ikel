<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maestro extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('session','form_validation','user_agent'));
        $this->load->helper('url', 'security');
        $this->load->model(array('mmaestro','musuario'));
	}
	public function index(){
		if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
			if($this->session->userdata('perfil')==2){
				$data_header["titulo"] = "index";
			    $data_header["descripcion"] = "";
			    $data_header["imagen"] = "";
			    $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
			    $data_header["cssExterno"] = array();
			    $data_javascript["script"] = array('jquery','materialize.min','flinpic','general','maestro/dashboard');
			    $data_javascript["scriptExterno"] = array();
			    $data_extras["contenido"] = ""; //Puede ser un array
			    
			    $id = $this->session->userdata('identificador');
			    $data_verCurso["categorias"] = $this->musuario->getCategorias();
			    $data_propuesta["propuestas"] = $this->mmaestro->getPropuestas($id);
			    $data_contenido["propuestaClase"] = $this->load->view('maestro/propuestaClase', $data_propuesta, TRUE);
			    if(is_numeric($this->uri->segment(3))){
			    	$curso = $this->uri->segment(3);
			    	$data_verClase["clases"] = $this->mmaestro->verClases($curso);
			    	$data_verClase["idCurso"] =$curso;
				}else{
			    	$data_verClase["cursos"] = $this->mmaestro->verCursos($id);
			    }
			    


			    $data_contenido["verClases"] = $this->load->view('maestro/verClases',$data_verClase, TRUE);

			    $data_verCurso["cursos"] = $this->mmaestro->verCursos($id);

			    $data_contenido["verCursos"] = $this->load->view('maestro/verCursos', $data_verCurso, TRUE);

			    $data["contenido"] = $this->load->view("maestro/index", $data_contenido , TRUE);
			    $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
			    $data["head"] = $this->load->view("head", $data_header, TRUE);
			    $data["menu"] = $this->load->view("menu", NULL , TRUE);
			    $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
			    $data["footer"] = $this->load->view("footer", NULL , TRUE);
			    $this->load->view("html",$data);
			}else{
				echo $this->session->userdata('perfil');
				echo "2";
				//redirect('acceso/login','refresh');
			}
		}else{
			echo "1";
			//redirect('acceso/login','refresh');
		}
		
	}

	public function solicitud(){
		if($this->input->is_ajax_request()){
			$data = $this->input->post();
			if($data!=NULL){
				foreach ($data as $key => $value) {
	                $data["$key"] = $this->security->xss_clean($value);
	            }
	            $this->form_validation->set_rules('Nombre','Nombre','trim|required|min_length[4]|max_length[30]');
                $this->form_validation->set_rules('ApellidoPaterno','Apellido Paterno','trim|required|min_length[4]|max_length[30]');
                $this->form_validation->set_rules('ApellidoMaterno','Apellido Materno','trim|required|min_length[4]|max_length[30]');
                $this->form_validation->set_rules('Prueba','Prueba','trim|required');
                $this->form_validation->set_rules('Categoria','Categoria','trim|required|numeric');
                
                if ($this->form_validation->run() != FALSE) {
                	if (preg_match('/v=/',$data['Prueba'])) {
                        $posicion = strpos($data["Prueba"],"v=");
                        $data["Prueba"] = substr($data["Prueba"],$posicion+2,strlen($data['Prueba']));

                    	$result = $this->mmaestro->solicitud($data);
	                    if ($result != FALSE) {
	                        $array = array("result" => TRUE);
	                    }else{
	                        $array = array("result" => FALSE,"error" => "Error al insertar los datos, vuelve a intentarlo");
	                    }
                    }else{
                    	$array = array("result" => FALSE,"error" => "Video no es de Youtube");
                    }

                    
                } else {
                    $errors = $this->form_validation->error_array();
                    $array = array("result" => FALSE,"error" => $errors);
                }
			}else{
				$array = array('result' => FALSE, 'error' => 'No se enviaron datos');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($array));
		}else{
			$data_header["titulo"] = "solicitud";
		    $data_header["descripcion"] = "";
		    $data_header["imagen"] = "";
		    $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
		    $data_header["cssExterno"] = array();
		    $data_javascript["script"] = array('jquery','materialize.min','flinpic','general','solicitud');
		    $data_javascript["scriptExterno"] = array();
		    $data_extras["contenido"] = ""; //Puede ser un array
		    $data["contenido"] = $this->load->view("maestro/solicitud", NULL , TRUE);
		    $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
		    $data["head"] = $this->load->view("head", $data_header, TRUE);
		    $data["menu"] = $this->load->view("menu", NULL , TRUE);
		    $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
		    $data["footer"] = $this->load->view("footer", NULL , TRUE);
		    $this->load->view("html",$data);
		}
	}	
	public function perfil(){
		if($this->uri->segment(3) != NULL  && is_numeric()){
			$id = $this->uri->segment(3);
			$datos = $this->mmaestro->getMaestro($id);
			if($datos != NULL){
				$data_header["titulo"] = "perfil";
				$data_header["descripcion"] = "";
				$data_header["imagen"] = "";
				$data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
				$data_header["cssExterno"] = array();
				$data_javascript["script"] = array('jquery','materialize.min','flinpic','general');
				$data_javascript["scriptExterno"] = array();
				$data_extras["contenido"] = ""; //Puede ser un array
				$data["contenido"] = $this->load->view("maestro/perfil", NULL , TRUE);
				$data["extras"] = $this->load->view("extras", $data_extras, TRUE);
				$data["head"] = $this->load->view("head", $data_header, TRUE);
				$data["menu"] = $this->load->view("menu", NULL , TRUE);
				$data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
				$data["footer"] = $this->load->view("footer", NULL , TRUE);
				$this->load->view("html",$data);
			}else{
				redirect("inicio","refresh");
			}
		}
	}	

	public function cargarCurso(){
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
				foreach ($data as $key => $value) {
					$data["$key"] = $this->security->xss_clean($value);
				}

                $this->form_validation->set_rules('Nombre','Nombre del Curso','trim|required|max_length[150]');
                $this->form_validation->set_rules('IDCategoria','Categoria','trim|required|numeric');
                $this->form_validation->set_rules('Descripcion','Descripcion','trim|required|max_length[150]');

				$data["IDMaestro"] = $this->session->userdata('identificador');
		        if ($this->form_validation->run() != FALSE) {
                    if(isset($data["ID"])){
                    	
                    	$id = $data["ID"];
                    	unset($data["ID"]);
                    	if($this->mmaestro->actualizarcurso($data,$id)){
		        			$array = array("result" => TRUE);
			        	}else{
			        		$array = array("result" => FALSE,"error"=> "Falla al registrar los datos, inténtalo de nuevo");
			        	}
                    }else{
                    	/*
                    	if($this->session->userdata('identificador')==3){
                    		$data["Habilitado"] = 2;
                    	}
                    ¨	*/
                    	if($this->mmaestro->nuevocurso($data)){
			        		$array = array("result" => TRUE);
			        	}else{
			        		$array = array("result" => FALSE,"error"=> "Falla al registrar los datos, inténtalo de nuevo");
			        	}	
                    }
                    
		        	
		        } else {
                    $errors = $this->form_validation->error_array();
		        	$array = array("result" => FALSE,"error" => $errors);
		        }

				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/POST nuevovideo
			}
		}
	}	
	public function eliminarCurso(){
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->get()==NULL){
				//Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                	$return = $this->mmaestro->eliminarCurso($id);
	                $array  = array("result" => $return);
					$this->output->set_content_type("application/json")->set_output(json_encode($array));	
                }
                //Fin AJAX/POST eliminarevento
			}
		}
	}
	public function verCurso(){
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			
				//Inicio AJAX/GET getevento
				if(is_numeric($id)&&$id>0){
					$array = $this->mmaestro->getCurso($id);
                    if ($array != FALSE) {
                        $array = $array["0"];
                    }else{
                        $array = "El curso no pudo ser obtenido, vuelve a intentarlo";
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
				}
				//Fin AJAX/GET getevento
			
		}
	}
	/*
	public function cargarTema(){
		$data_header["titulo"] = "nuevoTema";
	    $data_header["descripcion"] = "";
	    $data_header["imagen"] = "";
	    $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
	    $data_header["cssExterno"] = array();
	    $data_javascript["script"] = array('jquery','materialize.min','flinpic','general');
	    $data_javascript["scriptExterno"] = array();
	    $data_extras["contenido"] = ""; //Puede ser un array
	    $data["contenido"] = $this->load->view("maestro/vista", NULL , TRUE);
	    $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
	    $data["head"] = $this->load->view("head", $data_header, TRUE);
	    $data["menu"] = $this->load->view("menu", NULL , TRUE);
	    $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
	    $data["footer"] = $this->load->view("footer", NULL , TRUE);
	    $this->load->view("html",$data);
	}
	public function verTemas(){
		$data_header["titulo"] = "verTemas";
	    $data_header["descripcion"] = "";
	    $data_header["imagen"] = "";
	    $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
	    $data_header["cssExterno"] = array();
	    $data_javascript["script"] = array('jquery','materialize.min','flinpic','general');
	    $data_javascript["scriptExterno"] = array();
	    $data_extras["contenido"] = ""; //Puede ser un array
	    $data["contenido"] = $this->load->view("maestro/vista", NULL , TRUE);
	    $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
	    $data["head"] = $this->load->view("head", $data_header, TRUE);
	    $data["menu"] = $this->load->view("menu", NULL , TRUE);
	    $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
	    $data["footer"] = $this->load->view("footer", NULL , TRUE);
	    $this->load->view("html",$data);
	}
	public function verTema(){
		$data_header["titulo"] = "verTema";
	    $data_header["descripcion"] = "";
	    $data_header["imagen"] = "";
	    $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
	    $data_header["cssExterno"] = array();
	    $data_javascript["script"] = array('jquery','materialize.min','flinpic','general');
	    $data_javascript["scriptExterno"] = array();
	    $data_extras["contenido"] = ""; //Puede ser un array
	    $data["contenido"] = $this->load->view("maestro/vista", NULL , TRUE);
	    $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
	    $data["head"] = $this->load->view("head", $data_header, TRUE);
	    $data["menu"] = $this->load->view("menu", NULL , TRUE);
	    $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
	    $data["footer"] = $this->load->view("footer", NULL , TRUE);
	    $this->load->view("html",$data);
	}	
	public function eliminarTema(){
		$data_header["titulo"] = "eliminarTema";
	    $data_header["descripcion"] = "";
	    $data_header["imagen"] = "";
	    $data_header["css"] = array('materialize.min','font-awesome.min','paladium.min');
	    $data_header["cssExterno"] = array();
	    $data_javascript["script"] = array('jquery','materialize.min','flinpic','general');
	    $data_javascript["scriptExterno"] = array();
	    $data_extras["contenido"] = ""; //Puede ser un array
	    $data["contenido"] = $this->load->view("maestro/vista", NULL , TRUE);
	    $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
	    $data["head"] = $this->load->view("head", $data_header, TRUE);
	    $data["menu"] = $this->load->view("menu", NULL , TRUE);
	    $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
	    $data["footer"] = $this->load->view("footer", NULL , TRUE);
	    $this->load->view("html",$data);
	}
	*/
	//CLASES

	
	public function verClase(){
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			//Inicio AJAX/GET getevento
			if(is_numeric($id)&&$id>0){
				$array = $this->mmaestro->getCurso($id);
                if ($array != FALSE) {
                    $array = $array["0"];
                }else{
                    $array = "El curso no pudo ser obtenido, vuelve a intentarlo";
                }
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
			}
			//Fin AJAX/GET getevento
			
		}
	}

	public function cargarClase(){
		if($this->input->is_ajax_request()){
			$data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
			if($this->input->post()!=NULL){
				//Inicio AJAX/POST nuevovideo
				foreach ($data as $key => $value) {
					$data["$key"] = $this->security->xss_clean($value);
				}
				$this->form_validation->set_rules('IDCurso','ID Curso','trim|required');
                $this->form_validation->set_rules('Nombre','Nombre del Curso','trim|required|max_length[150]');
                $this->form_validation->set_rules('Contenido','Categoria','trim|required');
                $this->form_validation->set_rules('Descripcion','Descripcion','trim|required|max_length[150]');


		        if ($this->form_validation->run() != FALSE) {
		        	if (preg_match('/v=/',$data['Contenido'])) {
                        $posicion = strpos($data["Contenido"],"v=");
                        $data["Contenido"] = substr($data["Contenido"],$posicion+2,strlen($data['Contenido']));
                    }

                    $data_dos["Contenido"] = $data["Contenido"];
                    $data_dos["Tipo"] = 1;
                    
                    unset($data["Contenido"]);
                    $data["IDUsuario"] = $this->session->userdata('identificador');
                    if(isset($data["ID"])){
                    	$id = $data["ID"];
                    	unset($data["ID"]);
                    	if($this->mmaestro->actualizarclase($data,$id)){
		        			$array = array("result" => TRUE);
			        	}else{
			        		$array = array("result" => FALSE,"error"=> "Falla al registrar los datos, inténtalo de nuevo");
			        	}
                    }else{
                    	/*
                    	if($this->session->userdata('identificador')==3){
                    		$data["Habilitado"] = 2;
                    	}
                    ¨	*/
                    	$id = $this->mmaestro->nuevoclase($data);
                    	if($id!=FALSE){
			        		$this->mmaestro->nuevoContenido($id,$data_dos);
			        		$array = array("result" => TRUE);
			        	}else{
			        		$array = array("result" => FALSE,"error"=> "Falla al registrar los datos, inténtalo de nuevo");
			        	}	
                    }
                    
		        	
		        } else {
                    $errors = $this->form_validation->error_array();
		        	$array = array("result" => FALSE,"error" => $errors);
		        }

				$this->output->set_content_type("application/json")->set_output(json_encode($array));
				//Fin AJAX/POST nuevovideo
			}
		}
	}	
	public function elimiarClase(){
		if($this->input->is_ajax_request()){
			$id = $this->uri->segment(3);
			if($this->input->get()==NULL){
				//Inicio AJAX/POST eliminarevento
                if(is_numeric($id)&&$id>0){
                	$return = $this->mmaestro->eliminarClase($id);
	                $array  = array("result" => $return);
					$this->output->set_content_type("application/json")->set_output(json_encode($array));	
                }
                //Fin AJAX/POST eliminarevento
			}
		}
	}
}

/* End of file Maestro.php */
/* Location: ./application/controllers/Maestro.php */