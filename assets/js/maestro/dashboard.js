
$('body').delegate('[data-action]','click',function(event){
	event.preventDefault();
	var $s=$(this).data('action');
	$('.cont').hide();
	$('#'+$s).fadeIn(200);
});
$(".editarCurso").click(function() {
	var id = $(this).data("id");
	$("#EID").val(id);
	console.log(id);
	$.ajax({
		url: base_url+'maestro/verCurso/'+id,
		type: 'GET',
		data: {param1: 'value1'},
	})
	.done(function(data) {
		console.log(data);
		$("#ENombre").val(data.Nombre);
		$("#ECategoria > option[value='"+data.Categoria+"']").attr("selected","selected");
		$("#EDescripcion").val(data.Descripcion);
		console.log("success");

	})
	.fail(function(data) {
		console.log(data.responseText);
		console.log("error");
	});
	
});
$(".editarClase").click(function() {
	var id = $(this).data("id");
	$("#EIDC").val(id);
	console.log(id);
	$.ajax({
		url: base_url+'maestro/verCurso/'+id,
		type: 'GET',
		data: {param1: 'value1'},
	})
	.done(function(data) {
		console.log(data);
		$("#ENombre").val(data.Nombre);
		$("#ECategoria > option[value='"+data.Categoria+"']").attr("selected","selected");
		$("#EDescripcion").val(data.Descripcion);
		console.log("success");

	})
	.fail(function(data) {
		console.log(data.responseText);
		console.log("error");
	});
});
$("body").delegate(".eliminarCurso","click",function(){
//$(".eliminarevento").click(function(){
	var id = $(this).data("id");
	$('#ModalEliminar').openModal();
	$("#ModalAccion").text(1);
	$("#ModalElemento").text(id);
});
$("body").delegate(".eliminarClase","click",function(){
//$(".eliminarvideo").click(function(){
	var id = $(this).data("id");
	$('#ModalEliminar').openModal();
	$("#ModalAccion").text(2);
	$("#ModalElemento").text(id);
});
$("#confirmar").click(function(){
	var accion = $("#ModalAccion").text();
	var id = $("#ModalElemento").text();
	if(accion==1){
		$.ajax({
			url: base_url+'maestro/eliminarCurso/'+id,
			type: 'POST',
			data: {param1: '1'},
		})
		.done(function(data) {
			if(data.result==true){
				$("[data-curso='"+id+"']").remove();
				Materialize.toast("Eliminado", 1500, 'rounded');
					
			}else{
				console.log("No");
			}
		})
		.fail(function(data) {
			console.log(data.responseText);
			console.log("error");
		});
	}else if(accion ==2){
		$.ajax({
			url: base_url+'admin/eliminarClase/'+id,
			type: 'POST',
		})
		.done(function(data) {
			if(data.result==true){
				$("[data-clase='"+id+"']").remove();			 
				Materialize.toast("Eliminado", 1500, 'rounded');
			}else{
				console.log("No");
			}
		})
		.fail(function(data) {
			console.log(data.responseText);
			console.log("error");
		});
	}
});

$("#NuevoCurso").click(function(){
	var nombre = $("#Nombre").val();
	var categoria = $("#Categoria").val();
	var descripcion = $("#Descripcion").val();
	$.ajax({
		url: base_url+'maestro/cargarCurso',
		type: 'POST',
		data: {Nombre:nombre,IDCategoria:categoria,Descripcion:descripcion},
	})
	.done(function(data) {
		if(data.result==true){
			Materialize.toast("Creado", 1500, 'rounded');
			setInterval(function(){
				window.location=base_url+"maestro";
			}, 3000);
		}else{
			console.log(data);
			console.log("No");
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		console.log("error");
	});
	
});
$("#EditarCurso").click(function(){
	var nombre = $("#ENombre").val();
	var categoria = $("#ECategoria").val();
	var descripcion = $("#EDescripcion").val();
	var id = $("#EID").val();
	$.ajax({
		url: base_url+'maestro/cargarCurso',
		type: 'POST',
		data: {Nombre:nombre,IDCategoria:categoria,Descripcion:descripcion,ID:id},
	})
	.done(function(data) {
		if(data.result==true){
			Materialize.toast("Creado", 1500, 'rounded');
			setInterval(function(){
				window.location=base_url+"maestro";
			}, 3000);
		}else{
			console.log("No");
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		console.log("error");
	});
	
});

$("#IDCurso").change(function(){
	var valor = $(this).val();
	window.location=base_url+"maestro/index/"+valor;
});
$("#IDCurso2").change(function(){
	var valor = $(this).val();
	window.location=base_url+"maestro/index/"+valor;
});
$("#NuevaClase").click(function(){
	var descripcion  = $("#DescripcionClase").val();
	var url = $("#URL").val();
	var nombre = $("#NombreClase").val();
	var idCurso = $("#IDCurso").val();
	$.ajax({
		url: base_url+'maestro/cargarClase',
		type: 'POST',
		data: {Nombre:nombre,Contenido:url,Descripcion:descripcion,IDCurso:idCurso},
	})
	.done(function(data) {
		if(data.result==true){
			Materialize.toast("Creado", 1500, 'rounded');
			setInterval(function(){
				window.location=base_url+"maestro";
			}, 3000);
		}else{
			console.log(data);
			console.log("No");
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		console.log("error");
	});
});

$("#EditarClase").click(function(){
	var descripcion  = $("#DescripcionClase").val();
	var url = $("#EURL").val();
	var nombre = $("#ENombreClase").val();
	var idCurso = $("#EIDCurso").val();
	var idClase = $("#EIDClase").val();
	$.ajax({
		url: base_url+'maestro/cargarClase',
		type: 'POST',
		data: {Nombre:nombre,Contenido:url,Descripcion:descripcion,IDCurso:idCurso,ID:idClase},
	})
	.done(function(data) {
		if(data.result==true){
			Materialize.toast("Creado", 1500, 'rounded');
			setInterval(function(){
				window.location=base_url+"maestro";
			}, 3000);
		}else{
			console.log("No");
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		console.log("error");
	});
	
});