<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session','form_validation','user_agent'));
        $this->load->helper('url', 'security');
        $this->load->model('madmin');
        $this->CI = & get_instance();
    }

    public function index()
    {
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            redirect("admin/dashboard","refresh");
        }else{
            redirect("acceso/login","refresh");
        }
    }

           
    function logData(){
        
        date_default_timezone_set("America/Mexico_City");

        $times = $this->CI->db->query_times; 
        foreach ($this->CI->db->queries as $key => $query) { 
            if (!preg_match('/SELECT/', $query)) {
                $query = str_replace("INTO ", "", $query);
                $sql = explode(' ',$query);
                $data["IDUsuario"] = $this->session->userdata('identificador');
                $data["Accion"] = $sql[0];
                $data["Tabla"] = str_replace("`", "", $sql[1]);
                $data["IP"] = $this->input->ip_address();
                $patron = "/([0-9]+)/";
                if (preg_match($patron, $query,$id)) {
                    $data["IDElemento"] = $id[1];
                }else{
                    $data["IDElemento"] = 0;
                }                
                if ($this->agent->is_browser())
                {
                    $data["Navegador"] = $this->agent->browser().' '.$this->agent->version();
                }
                elseif ($this->agent->is_robot())
                {
                    $data["Navegador"] = $this->agent->robot();
                }
                elseif ($this->agent->is_mobile())
                {
                    $data["Navegador"] = $this->agent->mobile();
                }
                else
                {
                    $data["Navegador"] = 'OS sin identifiar';
                }
                $data["SistemaOperativo"] = $this->agent->platform();
                $data['Fecha'] = date('Y-m-d H:i:s');
                $this->macceso->setLog($data);
            }
                
        }

    }

    public function dashboard(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil') == 1){
            $data_head["titulo"]="Panel Administrador | IKEL";
            $data_head["descripcion"]="Todo el mundo tiene algo que ofrecer";
            $data_head["keywords"]="Ayuda, Dinero, Gratuito, Cursos, Aprender, Desempleo";
            $data_head["ico"]=base_url()."assets/img/ico.png";
            $data_head["ico"]=base_url("assets/img/ico.png");
            $data_head["css"]=array("materialize.min","font-awesome.min","paladium.min");

            $data_javascript["script"]=array("jquery","materialize.min","general.min","jquery.dataTables","zjs.utils","jQuery.dtplugin","admin/admin","admin/menu");
            
            $data_contenido["clases"] = $this->load->view('admin/rdclases', NULL, TRUE);
            $data_contenido["cursos"] = $this->load->view('admin/rdcursos', NULL, TRUE);
            $data_contenido["maestros"] = $this->load->view('admin/rdmaestros', NULL, TRUE);
            $data_contenido["temas"] = $this->load->view('admin/rdtemas', NULL, TRUE);
            $data["head"] = $this->load->view("head",$data_head,TRUE);
            $data["menu"]       = $this->load->view("menu","",TRUE);
            $data["javascript"] = $this->load->view("javascript",$data_javascript,TRUE);
            $data["contenido"]  = $this->load->view("admin/dashboard",$data_contenido,TRUE);
            $this->load->view("html",$data);
        }else{
            redirect("acceso/login","refresh");
        }
    }

    public function dataTable($tabla) {  
        $this -> load -> library('Datatable', array('model' => 'mpaginacion', 'rowIdCol' => 'ID'));
        $jsonArray = $this -> datatable -> datatableJson(
            array(
                'tabla' => $tabla
            )
        );
        $this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));
    }

    /*CRUD CATEGORIAS*/

    public function datosCategoria(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }

                    $this->form_validation->set_rules('Categoria','Categoría','trim|required|max_length[300]');
                    $this->form_validation->set_rules('Descripcion','Descripcion de la categoría','trim|required|min_length[10]');

                    if ($this->form_validation->run() != FALSE) {

                        if (isset($data['ID']) && !empty($data['ID'])) {
                            $id = $data['ID'];
                            unset($data['ID']);

                            $result = $this->madmin->editarCategoria($id,$data);       
                        }else{
                            $result = $this->madmin->nuevaCategoria($data);
                        }
      
                        if ($result != false) {
                            $array = array('result' => TRUE);
                            $this->logData();
                        }else{
                        //Hubo error al insertar/actualizar la categoria
                            $array = array('result' => FALSE, 'error' => 'Error al insertar los datos.');
                        }
                    }else{
                        //Error al validar los datos
                        $errors = $this->form_validation->error_array();
                        $array = array("result" => FALSE,"error" => $errors);
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }

    public function eliminarCategoria(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }


                    if (isset($data['ID']) && !empty($data['ID'])) {
                        $id = $data['ID'];
                        $result = $this->madmin->eliminarCategoria($id);       
                    }
  
                    if ($result != false) {
                        $this->logData();
                        $array = array('result' => TRUE);
                    }else{
                    //Hubo error al insertar/actualizar la categoria
                        $array = array('result' => FALSE, 'error' => 'Error al eliminar los datos.');
                    }

                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }
    
    public function getCategoria() {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getCategoria($id);
                    if ($array != FALSE) {
                        $array = $array["0"];
                    }else{
                        $array = "El registro no pudo ser obtenido, vuelve a intentarlo";
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }
    }

    public function getCategoriasDatatable(){
        if($this->input->is_ajax_request()){
            if($this->input->post()!=NULL){
                $this->dataTable("categorias c");
            }
        }
    }

    /*CRUD ADMINISTRADOR*/

    public function datosAdministrador(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }

                    $this->form_validation->set_rules('Nombres','Nombre completo','trim|required|min_length[4]|max_length[150]');
                    $this->form_validation->set_rules('Apellido Paterno','Apellido Paterno','trim|required|min_length[4]|max_length[150]');
                    $this->form_validation->set_rules('Apellido Materno','Apellido Materno','trim|min_length[4]|max_length[150]');
                    $this->form_validation->set_rules('Correo', 'Correo', 'trim|required|is_unique[usuarios.Correo]');
                    $this->form_validation->set_rules('Password','Contraseña','trim|required|min_length[8]|max_length[16]');
                    $this->form_validation->set_rules('Tipo','Tipo de cuenta','trim|required|numeric');

                    if ($this->form_validation->run() != FALSE) {

                        if (isset($data['ID']) && !empty($data['ID'])) {
                            $id = $data['ID'];
                            unset($data['ID']);

                            $result = $this->madmin->editarAdministrador($id,$data);       
                        }else{
                            $result = $this->madmin->nuevoAdministrador($data);
                        }
      
                        if ($result != false) {
                            $array = array('result' => TRUE);
                            $this->logData();
                        }else{
                        //Hubo error al insertar/actualizar la categoria
                            $array = array('result' => FALSE, 'error' => 'Error al insertar/actualizar los datos.');
                        }
                    }else{
                        //Error al validar los datos
                        $errors = $this->form_validation->error_array();
                        $array = array("result" => FALSE,"error" => $errors);
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }

    public function eliminarAdministrador(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }

                    if (isset($data['ID']) && !empty($data['ID'])) {
                        $id = $data['ID'];
                        $result = $this->madmin->eliminarAdministrador($id);       
                    }
  
                    if ($result != false) {
                        $array = array('result' => TRUE);
                        $this->logData();
                    }else{
                    //Hubo error al insertar/actualizar el administrador
                        $array = array('result' => FALSE, 'error' => 'Error al eliminar los datos.');
                    }

                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }

    public function getAdministrador() {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getAdministrador($id);
                    if ($array != FALSE) {
                        $array = $array["0"];
                    }else{
                        $array = "El registro no pudo ser obtenido, vuelve a intentarlo";
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }
    }

    public function getAdministradorDatatable(){
        if($this->input->is_ajax_request()){
            if($this->input->post()!=NULL){
                $this->dataTable("usuarios u",1);
            }
        }
    }

    /*READ & DELETE CURSOS*/

    public function getCurso()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                //Inicio AJAX/GET getevento
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getCurso($id);
                    if ($array != FALSE) {
                        $array = $array["0"];
                    }else{
                        $array = "El registro no pudo ser obtenido, vuelve a intentarlo";
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
                //Fin AJAX/GET getevento
            }
        }
    }

    public function getCursosDatatable(){
        if($this->input->is_ajax_request()){
            if($this->input->post()!=NULL){
                $this->dataTable("cursos cu");
            }
        }
    }

    public function eliminarCurso(){
        //if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }

                    if (isset($data['ID']) && !empty($data['ID'])) {
                        $id = $data['ID'];
                        $result = $this->madmin->eliminarCurso($id);       
                    }
  
                    if ($result != false) {
                        $array = array('result' => TRUE);
                        $this->logData();
                    }else{
                    //Hubo error al insertar/actualizar el administrador
                        $array = array('result' => FALSE, 'error' => 'Error al eliminar los datos.');
                    }

                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        
    }

    /*READ & DELETE MAESTRO*/

    public function getMaestro()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                //Inicio AJAX/GET getevento
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getMaestro($id);
                    if ($array != FALSE) {
                        $array = $array["0"];
                    }else{
                        $array = "El registro no pudo ser obtenido, vuelve a intentarlo";
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
                //Fin AJAX/GET getevento
            }
        }
    }

    public function getMaestrosDatatable(){
        if($this->input->is_ajax_request()){
            if($this->input->post()!=NULL){
                $this->dataTable("usuarios u");
            }
        }
    }

    public function eliminarMaestro(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }

                    if (isset($data['ID']) && !empty($data['ID'])) {
                        $id = $data['ID'];
                        $result = $this->madmin->eliminarMaestro($id);       
                    }
  
                    if ($result != false) {
                        $array = array('result' => TRUE);
                        $this->logData();
                    }else{
                    //Hubo error al insertar/actualizar el administrador
                        $array = array('result' => FALSE, 'error' => 'Error al eliminar los datos.');
                    }

                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }

    /*READ & DELETE TEMAS*/

    public function getTema()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                //Inicio AJAX/GET getevento
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getTemas($id);
                    if ($array != FALSE) {
                        $array = $array["0"];
                    }else{
                        $array = "El registro no pudo ser obtenido, vuelve a intentarlo";
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
                //Fin AJAX/GET getevento
            }
        }
    }

    public function getTemassDatatable(){
        if($this->input->is_ajax_request()){
            if($this->input->post()!=NULL){
                $this->dataTable("usuarios u");
            }
        }
    }

    public function eliminarTemas(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }

                    if (isset($data['ID']) && !empty($data['ID'])) {
                        $id = $data['ID'];
                        $result = $this->madmin->eliminarTemas($id);       
                    }
  
                    if ($result != false) {
                        $array = array('result' => TRUE);
                        $this->logData();
                    }else{
                    //Hubo error al insertar/actualizar el administrador
                        $array = array('result' => FALSE, 'error' => 'Error al eliminar los datos.');
                    }

                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }

    /*READ & DELETE Clases*/

    public function getClase()
    {
        if($this->input->is_ajax_request()){
            $id = $this->uri->segment(3);
            if($this->input->post()==NULL){
                //Inicio AJAX/GET getevento
                if(is_numeric($id)&&$id>0){
                    $array = $this->madmin->getClases($id);
                    if ($array != FALSE) {
                        $array = $array["0"];
                    }else{
                        $array = "El registro no pudo ser obtenido, vuelve a intentarlo";
                    }
                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
                //Fin AJAX/GET getevento
            }
        }
    }

    public function getClasessDatatable(){
        if($this->input->is_ajax_request()){
            if($this->input->post()!=NULL){
                $this->dataTable("usuarios u");
            }
        }
    }

    public function eliminarClases(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }

                    if (isset($data['ID']) && !empty($data['ID'])) {
                        $id = $data['ID'];
                        $result = $this->madmin->eliminarClases($id);       
                    }
  
                    if ($result != false) {
                        $array = array('result' => TRUE);
                        $this->logData();
                    }else{
                    //Hubo error al insertar/actualizar el administrador
                        $array = array('result' => FALSE, 'error' => 'Error al eliminar los datos.');
                    }

                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }
    

}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */ 