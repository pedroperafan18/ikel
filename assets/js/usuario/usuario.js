$(document).ready(function(){
	$("#datosactualizar").click(function{
		$.ajax({
			url: url+'usuario/actualizar/datos',
			type: 'GET',
		}).always(function(data) {
			if(data!=false){
				$("#Nombre").val(data.Nombre);
				$("#Paterno").val(data.ApellidoPaterno);
				$("#Materno").val(data.ApellidoMaterno);
				$("#Correo").val(data.Correo);
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
			console.log("complete");
		});
	});

	$("#actualizar").clcik(function{
		var nombre = $("#Nombre").val();
		var materno = $("#Materno").val();
		var paterno = $("#Paterno").val();
		var correo = $("#Correo").val();
		var pass = $("#Password").val();

		$.ajax({
			url: url+'usuario/actualizar',
			type: 'POST',
			data: {Nombre:nombre,Paterno:paterno,Materno:materno,Correo:correo,Password:pass},
		}).always(function(data) {
			if(data.result!=false){
				window.location=url+"usuario";
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
			console.log("complete");
		});
	});
});