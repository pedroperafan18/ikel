<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Minicio extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();		
	}
	public function masCursos($idUsuario,$num=0)
	{
		$this->db->select('cursos.ID, cursos.Nombre as NombreCurso, categorias.Nombre as NombreCategoria, cursos.Fotografia as Fotografia');
		$this->db->from('cursos');
		$this->db->join('gustosusuario', 'cursos.IDCategoria = gustosusuario.IDCategoria', 'inner');
		$this->db->join('categorias', 'gustosusuario.IDCategoria = categorias.ID', 'inner');
		$this->db->where('gustosusuario.IDUsuario', $idUsuario);
		$this->db->order_by('cursos.ID', 'desc');
		$this->db->limit(4);
		$this->db->offset($num);
		$query = $this->db->get();
		return (($query->num_rows()>0)?$query->result_array():NULL);
	}
	public function getVideo($num)
	{
		$query = $this->db->query("SELECT clases.Nombre AS NombreClase, contenido.Contenido as Contenido , clases.Descripcion, clases.Fecha , usuarios.Nombres, usuarios.ApellidoPaterno, usuarios.ApellidoMaterno FROM clases INNER JOIN contenido ON contenido.IDClase = clases.ID INNER JOIN usuarios ON usuarios.ID = clases.IDUsuario WHERE clases.ID = '$num'");
		//$this->db->select('clases.Nombre As NombreClases, clases.Descripcion, contenido.Contenido As Contenido');
		//$this->db->where('ID', $num);
		//$query = $this->db->get('clases');
		
		return (($query->num_rows()>0)?$query->result_array():NULL);
	}
	public function getCurso($num)
	{
		$this->db->where('ID', $num);
		$query = $this->db->get('cursos');
		return (($query->num_rows()>0)?$query->result_array():NULL);
	}
	public function getVideosCurso($num)
	{

		$this->db->where('IDCurso', $num);
		$query = $this->db->get('contenido');
		return (($query->num_rows()>0)?$query->result_array():NULL);
	}
	public function getSolicitudes($idUsuario)
	{
		$this->db->select('solicitud.ID, solicitud.NombreCurso, categorias.Nombre as NombreCategoria, solicitud.Prueba as Prueba');
		$this->db->from('solicitud');
		$this->db->join('gustosusuario', 'solicitud.Categoria = gustosusuario.IDCategoria', 'inner');
		$this->db->join('categorias', 'gustosusuario.IDCategoria = categorias.ID', 'inner');
		$this->db->where('gustosusuario.IDUsuario', $idUsuario);
		$this->db->order_by('solicitud.ID', 'desc');
		$this->db->limit(4);
		$query = $this->db->get();
		return (($query->num_rows()>0)?$query->result_array():NULL);
	}

	public function getResultados($parametro){
		$this->db->from('cursos');
		$this->db->like('Nombre',$parametro);
        $consulta = $this->db->get();
        return $consulta->result();
	}
	public function getContenidoCursos($id)
	{
		$query = $this->db->query("SELECT cursos.Nombre AS NombreCurso, clases.Nombre AS NombreClase, contenido.Contenido as Contenido ,clases.ID FROM cursos INNER JOIN clases ON cursos.ID = clases.IDCurso INNER JOIN contenido ON contenido.IDClase = clases.ID WHERE cursos.ID = '".$id."'");
		return (($query->num_rows()>0)? $query->result_array():FALSE);
	}

	public function setSuscripcion($data){
		if ($this->db->insert('inscripciones', $data)) {
			return TRUE;
		}else{
			return FALSE;
		}
		
	}

	public function setLog($data)
	{
		$this->db->insert('log',$data);
	}
}

/* End of file Minicio.php */
/* Location: ./application/models/Minicio.php */