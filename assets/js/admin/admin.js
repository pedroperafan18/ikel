$(document).ready(function(){   

    $("#cursosTabla").dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": base_url + "admin/getCursosDatatable",
            "type": "POST"
        },
        columns: [
            { data: "cu.ID" },
            { data: "cu.Nombre" },
            { data: "cu.FechaInicial" },
            { data: "cu.FechaFinal" },
            {
                data: "cu.ID",
          
            }
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [ {
            "render": function ( data, type, row ) {
                return '<a class="btn-flat eliminarcurso" data-id="'+data+'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            },
            "targets": 4,
            "bSortable": false
        } ],
    }).dataTableSearch(500);

    $("#maestrosTabla").dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": base_url + "admin/getMaestrosDatatable",
            "type": "POST"
        },
        columns: [
            { data: "u.ID" },
            { data: "u.Nombres" },
            { data: "u.ApellidoPaterno" },
            { data: "u.ApellidoMaterno" },
            { data: "u.Correo" },
            {
                data: "u.ID",
            }
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [ {
            "render": function ( data, type, row ) {
                return '<a class="btn-flat eliminarmaestro" data-id="'+data+'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            },
            "targets": 5,
            "bSortable": false
        } ],
    }).dataTableSearch(500); 

    $("#temasTabla").dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": base_url + "admin/getTemasDatatable",
            "type": "POST"
        },
        columns: [
            { data: "t.ID" },
            { data: "t.IDCurso" },
            { data: "t.Fecha" },,
            {
                data: "t.ID",
            }
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [ {
            "render": function ( data, type, row ) {
                return '<a class="btn-flat eliminartema" data-id="'+data+'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            },
            "targets": 3,
            "bSortable": false
        } ],
    }).dataTableSearch(500);

    $("#clasesTable").dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": base_url + "admin/getClaseDatatable",
            "type": "POST"
        },
        columns: [
            { data: "c.ID" },
            { data: "c.IDCurso" },
            { data: "c.Fecha" },,
            {
                data: "c.ID",
            }
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [ {
            "render": function ( data, type, row ) {
                return '<a class="btn-flat eliminarclase" data-id="'+data+'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            },
            "targets": 3,
            "bSortable": false
        } ],
    }).dataTableSearch(500); 

    $("body").delegate(".eliminarcurso","click",function(){
        
        var id = $(this).data("id");
        $('#ModalEliminar').openModal();
        $("#ModalAccion").text(1);
        $("#ModalElemento").text(id);
    });
    $("body").delegate(".eliminarmaestro","click",function(){
        
        var id = $(this).data("id");
        $('#ModalEliminar').openModal();
        $("#ModalAccion").text(2);
        $("#ModalElemento").text(id);
    });
    $("body").delegate(".eliminartema","click",function(){
        
        var id = $(this).data("id");
        $('#ModalEliminar').openModal();
        $("#ModalAccion").text(3);
        $("#ModalElemento").text(id);
    });
    $("body").delegate(".eliminarclase","click",function(){
        
        var id = $(this).data("id");
        $('#ModalEliminar').openModal();
        $("#ModalAccion").text(4);
        $("#ModalElemento").text(id);
    });
    
    $("#confirmar").click(function(){
        var accion = $("#ModalAccion").text();
        var id = $("#ModalElemento").text();
        if(accion==1){
            $.ajax({
                url: base_url+'admin/eliminarcurso',
                type: 'POST',
                data: {ID: id},
            })
            .done(function(data) {
                if(data.result==true){
                    var cursosTabla = $('#cursosTabla').DataTable();
                    cursosTabla.ajax.reload( function ( json ) {
                        Materialize.toast("Eliminado", 1500, 'rounded');
                    }); 
                }
                              
            })
            .fail(function(data) {
                console.log(data.responseText);
                console.log("error");
            });
        }else if(accion==2){
            $.ajax({
                url: base_url+'admin/eliminarmaestro',
                type: 'POST',
                data: {ID: id},
            })
            .done(function(data) {
                if(data.result==true){
                    var maestrosTabla = $('#maestrosTabla').DataTable();
                    maestrosTabla.ajax.reload( function ( json ) {
                        Materialize.toast("Eliminado", 1500, 'rounded');
                    }); 
                }
                              
            })
            .fail(function(data) {
                console.log(data.responseText);
                console.log("error");
            });
        }else if(accion==3){
            $.ajax({
                url: base_url+'admin/eliminartema',
                type: 'POST',
                data: {ID: id},
            })
            .done(function(data) {
                if(data.result==true){
                    var temasTabla = $('#temasTabla').DataTable();
                    temasTabla.ajax.reload( function ( json ) {
                        Materialize.toast("Eliminado", 1500, 'rounded');
                    }); 
                }
                              
            })
            .fail(function(data) {
                console.log(data.responseText);
                console.log("error");
            });
        }else if(accion==4){
            $.ajax({
                url: base_url+'admin/eliminarclase',
                type: 'POST',
                data: {ID: id},
            })
            .done(function(data) {
                if(data.result==true){
                    var clasesTabla = $('#clasesTabla').DataTable();
                    clasesTabla.ajax.reload( function ( json ) {
                        Materialize.toast("Eliminado", 1500, 'rounded');
                    }); 
                }
                              
            })
            .fail(function(data) {
                console.log(data.responseText);
                console.log("error");
            });
        }
    });
    

});
                
                
