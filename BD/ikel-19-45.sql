-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2016 a las 02:44:49
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ikel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificaciones`
--

CREATE TABLE `calificaciones` (
  `ID` int(10) NOT NULL,
  `IDClase` int(5) NOT NULL,
  `IDUsuario` int(5) NOT NULL,
  `Calificacion` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `ID` int(10) NOT NULL,
  `Nombre` varchar(150) NOT NULL,
  `Descripcion` text NOT NULL,
  `Habilitado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`ID`, `Nombre`, `Descripcion`, `Habilitado`) VALUES
(1, 'Uno', 'Unooooo', 1),
(2, 'Desarrollo Web', 'Desarrollo Web :D', 1),
(3, 'Desarrollo de Apliaciones', 'Desarrollo de Apliaciones :D', 1),
(4, 'Ingeniería de Software', 'Ingenieria de Software :D', 1),
(5, 'Desarrollo de Videojuegos', 'Desarrollo de Videojuegos :D', 1),
(6, 'Emprendimeinto', 'Emprendimeinto :D ', 1),
(7, 'Finanzas', 'Finanzas :D', 1),
(8, 'Fotografia', 'Fotografia :D', 1),
(9, 'Creatividad', 'Creatividad', 1),
(10, 'Oficios', 'Oficios :D', 1),
(11, 'Manualidades', 'Manualidades :D', 1),
(12, 'Idiomas', 'Idiomas :D', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE `clases` (
  `ID` int(10) NOT NULL,
  `IDCurso` int(5) NOT NULL,
  `IDUsuario` int(6) NOT NULL,
  `Nombre` varchar(150) NOT NULL,
  `Descripcion` text NOT NULL,
  `Fotografia` varchar(200) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Habilitado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clases`
--

INSERT INTO `clases` (`ID`, `IDCurso`, `IDUsuario`, `Nombre`, `Descripcion`, `Fotografia`, `Fecha`, `Habilitado`) VALUES
(1, 1, 4, 'Introducción', 'Introducción a C#', '', '2016-06-04 12:10:12', 1),
(2, 1, 4, 'Entorno Visual Studio', 'Entorno Visual Studio', '', '2016-06-04 12:12:22', 1),
(3, 1, 5, 'Tipos de Datos', 'Tipos de Datos', '', '2016-06-04 12:13:48', 1),
(4, 2, 4, 'Nueva Clase', 'Clase de prueba', '', '2016-06-04 19:16:54', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasesvistas`
--

CREATE TABLE `clasesvistas` (
  `ID` int(10) NOT NULL,
  `IDClase` int(10) NOT NULL,
  `IDUsuario` int(10) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `ID` int(8) NOT NULL,
  `IDClase` int(5) NOT NULL,
  `IDUsuario` int(5) NOT NULL,
  `Comentario` text NOT NULL,
  `Habilitado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE `contenido` (
  `ID` int(10) NOT NULL,
  `IDClase` int(5) NOT NULL,
  `Contenido` varchar(200) NOT NULL,
  `Tipo` tinyint(4) NOT NULL,
  `Habilitado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`ID`, `IDClase`, `Contenido`, `Tipo`, `Habilitado`) VALUES
(1, 1, 'UDVtMYqUAyw', 1, 1),
(2, 2, 'pQ6Ezq72J5A', 1, 1),
(3, 3, '_LUuWI-g_ew', 1, 1),
(4, 4, 'fmI_Ndrxy14', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenidoproyectos`
--

CREATE TABLE `contenidoproyectos` (
  `ID` int(10) NOT NULL,
  `IDProyecto` int(5) NOT NULL,
  `Contenido` varchar(300) NOT NULL,
  `Tipo` int(3) NOT NULL,
  `Habilitado` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `ID` int(5) NOT NULL,
  `Nombre` varchar(150) NOT NULL,
  `IDCategoria` int(5) NOT NULL,
  `Descripcion` text NOT NULL,
  `IDMaestro` int(10) NOT NULL,
  `Fotografia` varchar(200) NOT NULL,
  `FechaInicial` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `FechaFinal` datetime DEFAULT NULL,
  `Habilitado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`ID`, `Nombre`, `IDCategoria`, `Descripcion`, `IDMaestro`, `Fotografia`, `FechaInicial`, `FechaFinal`, `Habilitado`) VALUES
(1, 'Curso C#', 2, 'Curso de C# por Codigo Facilito', 4, '', '2016-06-04 12:02:45', NULL, 1),
(2, 'Nuevo', 2, 'dsdd', 4, '', '2016-06-04 16:41:00', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gustosusuario`
--

CREATE TABLE `gustosusuario` (
  `IDUsuario` int(5) NOT NULL,
  `IDCategoria` int(5) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gustosusuario`
--

INSERT INTO `gustosusuario` (`IDUsuario`, `IDCategoria`, `Fecha`) VALUES
(1, 2, '2016-06-04 11:13:28'),
(1, 4, '2016-06-04 11:13:28'),
(1, 5, '2016-06-04 11:13:28'),
(1, 8, '2016-06-04 11:13:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripciones`
--

CREATE TABLE `inscripciones` (
  `ID` int(10) NOT NULL,
  `IDUsuario` int(10) NOT NULL,
  `IDCurso` int(10) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Estatus` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `inscripciones`
--

INSERT INTO `inscripciones` (`ID`, `IDUsuario`, `IDCurso`, `Fecha`, `Estatus`) VALUES
(1, 1, 1, '2016-06-04 19:11:40', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `ID` int(11) NOT NULL,
  `IDUsuario` int(11) NOT NULL,
  `Accion` int(11) NOT NULL,
  `Tabla` int(11) NOT NULL,
  `IP` int(11) NOT NULL,
  `IDElemento` int(11) NOT NULL,
  `Navegador` int(11) NOT NULL,
  `SistemaOperativo` int(11) NOT NULL,
  `Fecha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`ID`, `IDUsuario`, `Accion`, `Tabla`, `IP`, `IDElemento`, `Navegador`, `SistemaOperativo`, `Fecha`) VALUES
(1, 1, 0, 0, 0, 1, 0, 0, 2016);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `ID` int(4) NOT NULL,
  `Nombre` varchar(150) NOT NULL,
  `IDUsuario` int(10) NOT NULL,
  `Descripcion` text NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Habilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `ID` int(5) NOT NULL,
  `Nombre` varchar(150) NOT NULL,
  `ApellidoPaterno` varchar(150) NOT NULL,
  `ApellidoMaterno` varchar(150) NOT NULL,
  `Correo` varchar(150) NOT NULL,
  `Prueba` varchar(300) NOT NULL,
  `NombreCurso` varchar(150) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Categoria` int(5) NOT NULL,
  `Revisado` tinyint(4) NOT NULL,
  `Habilitado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `solicitud`
--

INSERT INTO `solicitud` (`ID`, `Nombre`, `ApellidoPaterno`, `ApellidoMaterno`, `Correo`, `Prueba`, `NombreCurso`, `Fecha`, `Categoria`, `Revisado`, `Habilitado`) VALUES
(1, 'Pedro', 'Perafan', 'Carrasco', 'pedroperafan18@gmail.com', 'uLSvorP6QMo', 'CursoUno', '2016-06-04 01:59:01', 1, 0, 0),
(2, 'Pedro', 'Perafan', 'Carrasco', 'pedroperafan18@gmail.com', 'fmI_Ndrxy14', 'Nuevo', '2016-06-04 04:25:32', 2, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tema`
--

CREATE TABLE `tema` (
  `ID` int(6) NOT NULL,
  `IDCurso` int(5) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Habilitado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ID` int(5) NOT NULL,
  `Nombres` varchar(150) NOT NULL,
  `ApellidoPaterno` varchar(150) NOT NULL,
  `ApellidoMaterno` varchar(150) NOT NULL,
  `Correo` varchar(150) NOT NULL,
  `Password` varchar(300) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Tipo` int(2) NOT NULL COMMENT '1.- Admin ,2 .- Maestro,3 .- Alumno , 4.- Alumno y Maestro',
  `Habilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID`, `Nombres`, `ApellidoPaterno`, `ApellidoMaterno`, `Correo`, `Password`, `Fecha`, `Tipo`, `Habilitado`) VALUES
(1, 'Pedro', 'Perafan', 'Carrasco', 'pedroperafan18@gmail.com', '750158444d179ad8fcbeec0ff292455d61e86458', '2016-06-04 02:04:17', 3, 1),
(2, 'Pedro', 'Perafan', 'Carrasco', 'pedro_inat18@hotmail.com', 'd0e679f6c0ac1f58e3cdc1bccca1a95ca4a11f7f', '2016-06-04 09:34:36', 1, 1),
(3, 'Cristobal', 'Lopez', 'llamas', 'cris@hotmail.com', '750158444d179ad8fcbeec0ff292455d61e86458', '2016-06-04 10:59:04', 1, 1),
(4, 'Luis Gerardo', 'Serna', 'Carrizales', 'serna@hotmail.com', '750158444d179ad8fcbeec0ff292455d61e86458', '2016-06-04 10:59:54', 2, 1),
(5, 'Gerardo', 'Enriquez', 'Valdez', 'luis@hotmail.com', '750158444d179ad8fcbeec0ff292455d61e86458', '2016-06-04 10:59:54', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votos`
--

CREATE TABLE `votos` (
  `ID` int(10) NOT NULL,
  `IDSolicitud` int(5) NOT NULL,
  `IDUsuario` int(10) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `calificaciones`
--
ALTER TABLE `calificaciones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `clases`
--
ALTER TABLE `clases`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `clasesvistas`
--
ALTER TABLE `clasesvistas`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `contenidoproyectos`
--
ALTER TABLE `contenidoproyectos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `votos`
--
ALTER TABLE `votos`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `calificaciones`
--
ALTER TABLE `calificaciones`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `clases`
--
ALTER TABLE `clases`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `clasesvistas`
--
ALTER TABLE `clasesvistas`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `ID` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contenido`
--
ALTER TABLE `contenido`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `contenidoproyectos`
--
ALTER TABLE `contenidoproyectos`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tema`
--
ALTER TABLE `tema`
  MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `votos`
--
ALTER TABLE `votos`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
