<div class="wrapper">

    <div class="row busqueda-row">
        <div class="col s12 m12 l12 busqueda-col">
           <?php if(isset($resultados) && $resultados != NULL){
            echo'
            <div class="row busqueda-resultados-row">
                <div class="col s10 m10 l10 push-l1 push-s1 busqueda-resultados-col">

                    <div class="row">';
                        foreach ($resultados as $key) {
                            if ($key->Habilitado==1) {
                                echo'
                                    <div class="col s12 m4 l4 busqueda-objeto-col" data-evento="evento" data-evento-id="'.$key->ID.'">
                                        <div class="card">
                                            <div class="card-image">
                                               <h1>'.$key->Nombre.'</h1>
                                            </div>
                                            <div class="card-content">
                                                <p>Categoria</p>
                                            </div>
                                            <div class="card-action">
                                                <a href="'.base_url('inicio/curso/'.$key->ID.'').'">
                                                    <i class="fa fa-external-link" aria-hidden="true"></i>
                                                    Ver Curso
                                                </a>
                                                <br><br>
                                                <div data-id="'.$key->ID.'" class="bttn bttn-submit suscribirse">
                                                    Suscribirse
                                                </div>
                                            </div>  
                                        </div>                    
                                    </div>
                                ';
                            }
                        }   
                    echo'
                    </div>
                </div>
            </div>';
            }?>
        </div>
    </div>    
</div>