<div id="ModalEliminar" class="modal">
    <div class="modal-content">
        <h4>¿Está seguro de esta opción?</h4>
        <div class="hide" id="ModalAccion"></div>
        <div class="hide" id="ModalElemento"></div>
    </div>
    <div class="modal-footer">
        <a  href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
        <a id="confirmar" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Guardar cambios</a>
    </div>
</div>

<div class="container">
	<div class="row">
		<div class="col s3">
			<div class="collection">
		      <a href="#!" data-action="verCursos" class="collection-item">Ver Cursos</a>
		      <a href="#!" data-action="CrearCursos" class="collection-item">Crear Curso</a>
		      <a href="#!" data-action="verClases" class="collection-item">Ver Clases</a>
		      <a href="#!" data-action="crearClase" class="collection-item">Crear Clase</a>
		      <a href="#!" data-action="propuestaClase" class="collection-item">Propuesta de Clase</a>
		    </div>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		    <br>
		</div>
		<div class="col s9">
			<?php 
			echo $verClases;
			echo $verCursos; 
			echo $propuestaClase;
			?>
		</div>
	</div>
</div>
<style>
	.collection-item {
		cursor: pointer;
	}
	.cont{
		display: none
	}

</style>