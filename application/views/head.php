	<meta charset="UTF-8">
	<title><?php echo $titulo;?></title>
	<link rel="icon" href="<?php if(isset($ico)){echo $ico;}?>" type="image/png">
	<?php
	define("ABSOLUTOCSS",FCPATH."assets/css/");
	define("RELATIVOCSS","assets/css/");
	
	if(isset($css) && !empty($css)){
		foreach ($css as $key => $value) {
				$modulo = $this->uri->segment(1);
				$controlador = $this->uri->segment(2);
				if(file_exists(ABSOLUTOCSS.$modulo."/".$controlador."/".$value.".min.css")){
						echo "<link rel='stylesheet' href='".base_url(RELATIVOCSS.$modulo."/".$controlador."/".$value.".min.css")."' />\n";
				}elseif (file_exists(ABSOLUTOCSS.$modulo."/".$value.".min.css")) {
						echo "<link rel='stylesheet' href='".base_url(RELATIVOCSS.$modulo."/".$value.".min.css")."' />\n";
				}elseif(file_exists(ABSOLUTOCSS.$value.".min.css")){
						echo "<link rel='stylesheet' href='".base_url(RELATIVOCSS.$value.".min.css")."' />\n";
				}elseif(file_exists(ABSOLUTOCSS.$modulo."/".$controlador."/".$value.".css")){
						echo "<link rel='stylesheet' href='".base_url(RELATIVOCSS.$modulo."/".$controlador."/".$value.".css")."' />\n";
				}elseif (file_exists(ABSOLUTOCSS.$modulo."/".$value.".css")) {
						echo "<link rel='stylesheet' href='".base_url(RELATIVOCSS.$modulo."/".$value.".css")."' />\n";
				}elseif(file_exists(ABSOLUTOCSS.$value.".css")){
						echo "<link rel='stylesheet' href='".base_url(RELATIVOCSS.$value.".css")."' />\n";
				}else{
					echo "<link rel='stylesheet' href='".base_url(RELATIVOCSS.$value.".css")."' />\n";
					echo "<script>console.log('No existe el archivo css ".$value.".css');</script>\n";
				}
		}
	}
	if(isset($cssExterno) && !empty($cssExterno)){
		foreach ($cssExterno as $key => $value) {
			echo "<link rel='stylesheet' href='".$value."' />\n";
		}
	}
	?>
