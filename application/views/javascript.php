<div id="ModalEliminar" class="modal">
    <div class="modal-content">
        <h4>¿Está seguro de esta opción?</h4>
        <div class="hide" id="ModalAccion"></div>
        <div class="hide" id="ModalElemento"></div>
    </div>
    <div class="modal-footer">
        <a  href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
        <a id="confirmar" href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Guardar cambios</a>
    </div>
</div>
<script type="text/javascript">
  var base_url = "<?php echo base_url();?>";
  var token = "<?php echo $this->session->userdata('token');?>";
</script>
<?php
    define("ABSOLUTOJS",FCPATH."assets/js/");
    define("RELATIVOJS","assets/js/");

    if(isset($script)&& !empty($script)){
      foreach ($script as $key => $value) {
          $modulo = $this->uri->segment(1);
          $controlador = $this->uri->segment(2);
          if(file_exists(ABSOLUTOJS.$modulo."/".$controlador."/".$value.".min.js")){
              echo "<script src='".base_url(RELATIVOJS.$modulo."/".$controlador."/".$value.".min.js")."'></script>\n";
          }elseif (file_exists(ABSOLUTOJS.$modulo."/".$value.".min.js")) {
              echo "<script src='".base_url(RELATIVOJS.$modulo."/".$value.".min.js")."'></script>\n";
          }elseif(file_exists(ABSOLUTOJS.$value.".min.js")){
              echo "<script src='".base_url(RELATIVOJS.$value.".min.js")."'></script>\n";
          }elseif(file_exists(ABSOLUTOJS.$modulo."/".$controlador."/".$value.".js")){
              echo "<script src='".base_url(RELATIVOJS.$modulo."/".$controlador."/".$value.".js")."'></script>\n";
          }elseif (file_exists(ABSOLUTOJS.$modulo."/".$value.".js")) {
              echo "<script src='".base_url(RELATIVOJS.$modulo."/".$value.".js")."'></script>\n";
          }elseif(file_exists(ABSOLUTOJS.$value.".js")){
              echo "<script src='".base_url(RELATIVOJS.$value.".js")."'></script>\n";
          }else{
            echo "<script>console.log('No existe el archivo ".$value.".js');</script>\n";
          }
      }
    }

    if(isset($scriptExterno)&& !empty($scriptExterno)){
      foreach ($scriptExtreno as $key => $value) {
          echo "<script src='".$value."'></script>";
      }
    }
?>
