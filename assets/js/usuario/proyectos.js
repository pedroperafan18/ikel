$(document).ready(function(){

	$("#nuevoproyecto").click(function(){
		var nombre = $("#Nombre").val();
		var descripcion = $("#Descripcion").val();
		var url = $("#Url").val();
		var fd = new FormData();
		fd.append('Nombre',nombre);
		fd.append('Contenido',url);
		fd.append('Descripcion',descripcion);
		$.ajax({
			url: base_url+"proyectos/crearProyecto",
			type:"POST",
			contentType: false,
			data: fd,
			processData: false,
			cache: false,
		}).always(function(data){		
			console.log(data.result);
			console.log(data.statustext);
			if(data.result!=false){
				window.location=base_url+"proyectos/";
			}else{
				
			}
			if(typeof data.responseText != "undefined"){
				console.log(data.responseText);
				Materialize.toast("Error!", 3000, 'rounded');
			}
		});
	});

	$("#actualizarproyecto").click(function(){
		var nombre = $("#Anombre").val();
		var descripcion = $("#Adescripcion").val();
		var url = $("#Aurl").val();
		var id = $(this).data("id");
		var fd = new FormData();
		
		fd.append('Nombre',nombre);
		fd.append('Contenido',url);
		fd.append('Descripcion',descripcion);
		fd.append("userfile", document.getElementById('contenido').files[0]);
		$.ajax({
			url: base_url+"proyectos/crearProyecto/"+id,
			type:"POST",
			contentType: false,
			data: fd,
			processData: false,
			cache: false,
		}).always(function(data){		
			console.log(data);
			if(data.Resultado!=true){
				console.log(data.Resultado);
			}else{
				
			}
			if(typeof data.responseText != "undefined"){
				console.log(data.responseText);
				Materialize.toast("Error!", 3000, 'rounded');
			}
		});
	});

	$("#eliminarproyecto").click(function(){
		var id = $(this).data("id");
		$.ajax({
			url: base_url+"proyectos/eliminarProyecto/"+id,
			type:"POST",
		}).always(function(data) {
			if(data.result!=false){
				console.log("complete");
			}else{
				$.each(data.error, function(index, val) {
					Materialize.toast(val, 12000, 'rounded');
				});
			}
		});
	});
	$("#comentar").click(function(){
		var id = $(this).data("id");
		var comentario = $("#comentario").val();
		$.ajax({
			url: base_url+"proyectos/comentar/"+id,
			type:"POST",
			data:{Comentario:comentario},
		}).always(function(data) {
			if(data.result!=false){
				console.log(data.result);
				window.location=base_url+"proyectos/proyecto/"+id;
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
		});
	});
});