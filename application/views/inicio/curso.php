<div class="wrapper">
	<div class="row curso-row">
		<div class="col s12 m12 l12 curso-col skew-top-cw skew-bottom-cw">
			<div class="row curso-contenido-row">
				<div class="col s12 m12 l12 curso-contenido-col">
					<div class="row">
					<?php 
					if(isset($curso)){
						$i=1;
                    	echo'<div class="row">';
                    	if(is_array($columnas)){
                    		foreach($columnas as $key => $value){
	                    	echo '
							<div class="col s12 m12 l3 curso-picture" style="background:url('.base_url('assets/img/'.$value['img'].'').');background-size: cover;">
								<div class="label-picture">
									<p>Curso '.$i.'</p>
								</div>
							</div>';
							if($i<4){
	                            $i++;
	                        }else{
	                            echo'</div><div class="row">';
	                            $i=1;
	                        }
							}
                    	}
                    	
					}
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>