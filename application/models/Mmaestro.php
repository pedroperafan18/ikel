<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mmaestro extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();		
	}
	public function solicitud($data)
	{
		return $this->db->insert('solicitud', $data);
	}
	public function verCursos($id)
	{
		$this->db->where('IDMaestro', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('cursos');
		return (($query->num_rows()>0)? $query->result_array():FALSE);
	}
	public function eliminarCurso($id)
	{
		$this->db->where('ID', $id);
		if($this->db->update('cursos', array("Habilitado"=>"0"))){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevocurso($data)
	{
		return $this->db->insert('cursos', $data);
	}
	public function actualizarcurso($data,$id)
	{
		$this->db->where('ID', $id);
		return $this->db->update('cursos', $data);
	}
	public function getCurso($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('cursos');
		return (($query->num_rows()>0)? $query->result_array():FALSE);
	}
	public function verClases($id)
	{
		
		$this->db->where('IDCurso', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('clases');
		return (($query->num_rows()>0)? $query->result_array():FALSE);
	}
	public function nuevoclase($data)
	{
		$this->db->insert('clases', $data);
		return $this->db->insert_id();
	}
	public function getPropuestas($id)
	{
		$query = $this->db->query("SELECT cursos.Nombre AS NombreCurso, clases.Nombre AS NombreClase, contenido.Contenido as contenido, usuarios.Nombres, usuarios.ApellidoPaterno FROM cursos INNER JOIN clases ON cursos.ID = clases.IDCurso INNER JOIN contenido ON contenido.IDClase = clases.ID INNER JOIN usuarios ON clases.IDUsuario = usuarios.ID WHERE cursos.IDMaestro = '$id' AND clases.IDUsuario != '$id'");
		return (($query->num_rows()>0)? $query->result_array():FALSE);
	}
}

/* End of file Mmaestro.php */
/* Location: ./application/models/Mmaestro.php */