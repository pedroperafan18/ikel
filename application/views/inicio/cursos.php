	<?php
	$rands = array(
		'#F44336',
		'#E91E63',
		'#9C27B0',
		'#673AB7',
		'#3F51B5',
		'#2196F3',
		'#03A9F4',
		'#00BCD4',
		'#009688',
		'#4CAF50',
		'#8BC34A',
		'#CDDC39',
		'#FFEB3B',
		'#FFC107',
		'#FF9800',
		'#FF5722',
		'#795548',
		'#607D8B'
	);

	?>
<div class="wrapper">
	<div class="row parallax-row">
		<div class="col s12 m12 l12 parallax-col parallax-b1 skew-top-cw skew-bottom-cw">
			<div class="row" style="margin-top:150px">
				<div class="col s12 m12 l12">
					<div class="row" >
					<?php
						if(is_array($videos)){
							$j=1;
                    			foreach($videos as $value){
                    					echo '
                    					<a href="'.base_url('inicio/video/'.$value['ID'].'').'">
										<div class="col s12 m12 l3 curso-picture" style="background:'.$rands[rand(0,17)].';background-size: 100% 100%;margin:10px;padding:5px;text-align:center">
											<div class="label-picture">
												<p> Nombre de Video :'.$value['NombreClase'].'</p>
											</div>
										</div></a>';
										if($j<4){
                            			$j++;
                        				}else{
                            				echo'</div><div class="row">';
                            				$j=1;
                        				}
                        				
										}
									}else{
										echo '<h4>No hay material dentro del curso</h4>';
									}
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>