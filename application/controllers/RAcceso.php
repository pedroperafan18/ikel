<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acceso extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('session','form_validation','user_agent'));
        $this->load->helper('url', 'security');
        $this->load->model('macceso');
	}

	public function index()
	{
	 	if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
	 		$perfil = $this->session->userdata('perfil');
        	if ($perfil == 1) {
            	$perfil = "Administrador";
            }elseif ($perfil == 2 || $perfil == 4) {
            	$perfil = "Maestro";
            }elseif ($perfil == 3) {
            	$perfil = "Usuario";
            }
            redirect($perfil."/dashboard","refresh");
        }else{
            redirect("acceso/login","refresh");
        }
	}

	function logData(){
        
        date_default_timezone_set("America/Mexico_City");

        $times = $this->CI->db->query_times; 
        
        foreach ($this->CI->db->queries as $key => $query) { 
            if (!preg_match('/SELECT/', $query)) {
                $query = str_replace("INTO ", "", $query);
                $sql = explode(' ',$query);
                $data["IDUsuario"] = $this->session->userdata('identificador');
                $data["Accion"] = $sql[0];
                $data["Tabla"] = str_replace("`", "", $sql[1]);
                $data["ExTime"] = $times[$key];
                $data["IP"] = $this->input->ip_address();
                $patron = "/([0-9]+)/";
                if (preg_match($patron, $query,$id)) {
                    $data["IDElemento"] = $id[1];
                }else{
                    $data["IDElemento"] = 0;
                }                
                if ($this->agent->is_browser())
                {
                    $data["Navegador"] = $this->agent->browser().' '.$this->agent->version();
                }
                elseif ($this->agent->is_robot())
                {
                    $data["Navegador"] = $this->agent->robot();
                }
                elseif ($this->agent->is_mobile())
                {
                    $data["Navegador"] = $this->agent->mobile();
                }
                else
                {
                    $data["Navegador"] = 'OS sin identifiar';
                }
                $data["SistemaOperativo"] = $this->agent->platform();
                $data['Fecha'] = date('Y-m-d H:i:s');
                $this->macceso->setLog($data);
            }
                
        }

    }

	public function login(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
        	$perfil = $this->session->userdata('perfil');
        	if ($perfil == 1) {
            	$perfil = "Administrador";
            }elseif ($perfil == 2 || $perfil == 4) {
            	$perfil = "Maestro";
            }elseif ($perfil == 3) {
            	$perfil = "Usuario";
            }
            redirect($perfil."/dashboard","refresh");
        }else{
            if($this->input->is_ajax_request()!=FALSE){
                $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
                if($this->input->post()!=NULL){
                	foreach ($data as $key => $value) {
	                    $data["$key"] = $this->security->xss_clean($value);
	                }
                    $this->form_validation->set_rules('Correo', 'Correo electrónico', 'trim|required|valid_email');
                    $this->form_validation->set_rules('Password', 'Contraseña', 'trim|required|min_length[8]|max_length[16]');
                    if ($this->form_validation->run() == FALSE) {
                        $errors = $this->form_validation->error_array();
                   		$array = array("result" => FALSE,"error" => $errors);
                    }else {
                        $data["Password"] = sha1(md5($data["Password"]));
                        $result = $this->macceso->login($data);
                        if($result!=FALSE){
                            $id = $result["0"]["ID"];
                            $perfil = $result["0"]["Tipo"];
                            $array = array(
                                'identificador' => $id,
                                'perfil'    => $perfil
                            );
                            $this->session->set_userdata($array);
                            $array = array('result' => TRUE);
                        }else{
                        	$array = array('result' => FALSE, 'error' => 'Usuario / Contraseña incorrecto(s)');
                        }  
                    }
                }
                $this->output->set_content_type("application/json")->set_output(json_encode($array));
            }else{
            	redirect('acceso/login/','refresh');
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('identificador');
        $this->session->unset_userdata('perfil');
        redirect('acceso/login','refresh');
    }

    public function registro(){
    	if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
        	$perfil = $this->session->userdata('perfil');
        	if ($perfil == 1) {
            	$perfil = "Administrador";
            }elseif ($perfil == 2 || $perfil == 4) {
            	$perfil = "Maestro";
            }elseif ($perfil == 3) {
            	$perfil = "Usuario";
            }
            redirect($perfil."/dashboard","refresh");
        }else{
	        if($this->input->is_ajax_request()){
	            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
	            if($this->input->post()!=NULL){
	            	foreach ($data as $key => $value) {
	                    $data["$key"] = $this->security->xss_clean($value);
	                }
	                
	                $this->form_validation->set_rules('Nombres','Nombre completo','trim|required|min_length[4]|max_length[150]');
	                $this->form_validation->set_rules('Apellido Paterno','Apellido Paterno','trim|required|min_length[4]|max_length[150]');
	                $this->form_validation->set_rules('Apellido Materno','Apellido Materno','trim|min_length[4]|max_length[150]');
	                $this->form_validation->set_rules('Correo', 'Correo', 'trim|required|is_unique[usuarios.Correo]');
	                $this->form_validation->set_rules('Password','Contraseña','trim|required|min_length[8]|max_length[16]');
	                $this->form_validation->set_rules('Tipo','Tipo de cuenta','trim|required|numeric');
	                if ($this->form_validation->run() != FALSE) {
	                	date_default_timezone_set('America/Mexico_City');
	                    $data["Password"] = sha1(md5($data["Password"]));
	                    $result = $this->macceso->nuevousuario($data);
	                    if ($result != FALSE) {
	                        $array = array("result" => TRUE);
	                    }else{
	                        $array = array("result" => FALSE,"error" => "Error al insertar los datos, vuelve a intentarlo");
	                    }
	                } else {
	                    $errors = $this->form_validation->error_array();
	                    $array = array("result" => FALSE,"error" => $errors);
	                }
	                $this->output->set_content_type("application/json")->set_output(json_encode($array));
	            }
	        }
	    }
    }

    public function gustos(){
    	if(!$this->session->userdata('identificador') || !$this->session->userdata('perfil')){
        	redirect("acceso/login","refresh");
        }else{
	        if($this->input->is_ajax_request()){
	            $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
	            if($this->input->post()!=NULL){
	            	foreach ($data as $key => $value) {
	                    $data["$key"] = $this->security->xss_clean($value);
	                }

	                $categorias=explode(',',$data['Categorias']);
	                $data = "";
	                $result = TRUE
	                $data["ID"] = $this->session->userdata('identificador');
	                for($i=0;$i<count($categorias);$i++) {
	                	$data["Categoria"] = $categorias[$i];
	                    $result = $this->madmin->cargarcaracteristicas($data);
	                    if($result==FALSE)	{
	                    	$result = FALSE;
	                    }
	                }
                    if ($result != FALSE) {
                        $array = array("result" => TRUE);
                    }else{
                        $array = array("result" => FALSE,"error" => "Error al insertar los datos, vuelve a intentarlo");
                    }
	                $this->output->set_content_type("application/json")->set_output(json_encode($array));
	            }
	        }
	    }
    }



}

/* End of file acceso.php */
/* Location: ./application/controllers/acceso.php */ 