<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('musuario');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index() {
        if($this->session->userdata('identificador')){
            redirect('usuario/perfil','refresh');
        }
        else{
            // Redirigir cuando no hay sesion.
            redirect('inicio','refresh');
        }
    }

    public function perfil(){
        if($this->session->userdata('identificador')){


            $id = $this->session->userdata('identificador');
            // Manda el id del usuario que inicio sesion y el tipo para traer sus datos.
            $datos_usuario['usuario'] = $this->musuario->getUsuario($id,3);    


            if($this->musuario->getInscripcion($id)!=FALSE){
                $inscripciones=$this->musuario->getInscripcion($id);
                $i=0;
                foreach ($inscripciones as $key => $value){
                    if($this->musuario->getCurso($id)!=FALSE){
                        $datos_usuario['cursos'][$i] =  $this->musuario->getCurso($value['IDCurso']);
                        $i++;
                    }
                }
            }else{
                $datos_usuario['cursos']=NULL;
            }

            $datos_usuario['proyectos'] = ($this->musuario->getProyectos($id)!=FALSE)?$this->musuario->getProyectos($id):NULL;

            $data_head["titulo"]="IKEL";
            $data_head["descripcion"]="Todos tienen algo que aportar.";
            $data_head["keywords"]="";
            $data_head["ico"]=base_url("img/ico.png");
            $data_head["css"]=array("materialize.min","font-awesome.min","paladium.min");
            $data_javascript["script"]=array("jquery","materialize.min","inicio/inicio.min");

            $data["head"]       = $this->load->view("head",$data_head,TRUE);  
            $data["javascript"] = $this->load->view("javascript",$data_javascript,TRUE);
            $data["menu"]       = $this->load->view("menu","",TRUE);
            $data["footer"]     = $this->load->view("footer","",TRUE);
            $data["contenido"]  = $this->load->view("usuario/perfil",$datos_usuario,TRUE);
            $this->load->view("html",$data);
        }else{
            redirect('inicio/inicio','refresh');
        }
    }

    public function comentario(){
        if($this->input->is_ajax_request()){
            if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
                $data = ($this->input->post() == NULL) ? $this->input->get() : $this->input->post();
                if($this->session->post() != NULL){
                    foreach ($data as $key => $value) 
                        $data["$key"] = $this->security->xss_clean($value);
                    $this->form_validation->set_rules('Comentario', 'Comentario', 'trim|required|max_length[500]');
                    if($this->form_validation->run() == FALSE){
                        $errors = $this->form_validation->error_array();
                        $array = array('result' => FALSE, "error" => $errors);
                    }
                    else{
                        if($this->uri->segment(3)){
                            $data['IDClase'] = $this->uri->segment(3);
                            $data['IDUsuario'] = $this->session->userdata('identificador');
                            if(is_numeric($data['IDClase']) && is_numeric($data['IDUsuario'])){
                                // Se le pasa al modelo un data esperando recibir un true o false.
                                $result = $this->musuario->setComentario($data);
                            }
                            if($result == TRUE){
                                $array = array('result' => $result);
                            }
                            else{
                                $array = array('result' => $result, 'error' => "Error al ingresar el Comentario.");
                            }
                        }
                        else{
                            $array = array('result' => $result, 'error' => "Error al encontrar video de la clase.");
                        }
                    }
                    $this->output->set_content_type('application/json')->set_output(json_encode($array));
                }
            }
            else{
                // No hay sesion.
                redirect('inicio','refresh');      
            }
        }
    }

    public function inscribir() {
    	if($this->input->is_ajax_request()){
            if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
                $data = $this->input->post();
                if($data != NULL){
                    foreach ($data as $key => $value){
                        $data["$key"] = $this->security->xss_clean($value);
                    }
                    if($this->uri->segment(3)){
                        $data['IDUsuario'] = $this->session->userdata('identificador');
                        $data['IDCurso'] = $this->uri->segment(3);
                        if(is_numeric($data['IDUsuario']) && is_numeric($data['IDCurso'])){
                            $result = $this->musuario->setInscribir($data);
                            if($result == TRUE)
                            {
                                $array = array('result' => $result);
                            }
                            else
                            {
                                $array = array('result' => $result, 'error' => "Error al inscribirte.");
                            }
                        }
                    }
                    else{
                        // Error al inscribirte.
                        $array = array('result' => $result, 'error' => "Error al inscribirte.");
                    }
                    $this->output->set_content_type('application/json')->set_output(json_encode($array));
                }
            }
        }
    }

    public function calificarC() {
        if($this->input->is_ajax_request()){
            if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
                $data = ($this->input->post() == NULL) ? $this->input->get() : $this->input->post();
                if($this->input->post() != NULL){
                    foreach ($data as $key => $value) 
                    {
                        $data["$key"] = $this->security->xss_clean($value);
                    }
                    $data['IDClase'] = $this->uri->segment(3);
                    $data['IDUsuario'] = $this->session->userdata('identificador');
                    $this->form_validation->set_rules('Calificacion', 'Calificacion', 'integer|numeric|is_natural_no_zero');
                    if ($this->form_validation->run() == FALSE) {
                        $errors = $this->form_validation->error_array();
                        $array = array('result' => FALSE, "error" => $errors);
                    } else {
                        if(is_numeric($data['IDClase']) && is_numeric($data['IDUsuario'])){
                            $result = $this->musuario->setCalificarC($data);
                            if($result == TRUE){
                                $array = array('result' => $result);
                            }
                            else{
                                $array = array('result' => $result, 'error' => "Error al intentar calificar.");
                            }   
                        }
                    }
                    $this->output->set_content_type('application/json')->set_output(json_encode($array));
                }
            }
            else{
                // No hay sesion.
                redirect('inicio/inicio','refresh');
            }
        }
    }

    public function calificarM() {
        # code... // Pendiente.
    }
    public function gustos(){
        if($this->session->userdata('identificador')){
            $id = $this->session->userdata('identificador');
            $data_contenido["categorias"] = $this->musuario->getCategorias();
            $data_contenido["gustos"] = $this->musuario->getGustos($id);
            $data_header["titulo"] = "gustos";
            $data_header["descripcion"] = "";
            $data_header["imagen"] = "";
            $data_header["cssExterno"] = array();
            $data_header["css"]=array("materialize.min","font-awesome.min","paladium.min");
            $data_javascript["script"]=array("jquery","materialize.min","general.min","inicio/acceso");
            $data_javascript["scriptExterno"] = array();
            $data_extras["contenido"] = ""; //Puede ser un array
            $data["contenido"] = $this->load->view("usuarios/gustos", $data_contenido , TRUE);
            $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
            $data["head"] = $this->load->view("head", $data_header, TRUE);
            $data["menu"] = $this->load->view("menu", NULL , TRUE);
            $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
            $data["footer"] = $this->load->view("footer", NULL , TRUE);
            $this->load->view("html",$data);
        }else{
            redirect('acceso','refresh');   
        }
    }    
}