<input type="hidden" id="IDCurso" value="<?php if(isset($idCurso)){ echo $idCurso; }?>" >
<div id="verClases" class="row cont">
	<?php
	if(isset($clases) && is_array($clases)){
	?>
	<table>
		<thead>
			<tr>
				<th data-field="id">IDClase</th>
				<th data-field="nombre">Nombre</th>
				<th data-field="editar"></th>
				<th data-field="eliminar"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		if(is_array($clases)){
			foreach ($clases as $key => $clase) {
			?>
			<tr data-curso="<?=$clase["ID"]?>">
				<td><?=$clase["ID"]?></td>
				<td><?=$clase["Nombre"]?></td>
				<td><div class="btn blue editarClase" data-action="editarClase" data-id="<?=$clase["ID"]?>">EDITAR</div></td>
				<td><div class="btn red eliminarClase" data-id="<?=$clase["ID"]?>">ELIMINAR</div></td>
			</tr>
			<?php
			}
		}
		?>	
		</tbody>
	</table>
	<?php
	}else{
	?>
	<div class="input-field col s12">
	<select id="IDCurso">
	<option value="" disabled selected>Elige el curso</option>
	<?php
	if(is_array($cursos)){
		foreach ($cursos as $key => $curso) {
		?>
		<option value='<?=$curso["ID"]?>'><?=$curso["Nombre"]?></option>
		<?php
		}
	}
	?>

	</select>
	<label>Elige el curso</label>
	</div>
	<?php
	}
	?>
</div>
<div id="crearClase" class="row cont">
	<div class="row">
	<?php
if(isset($cursos)){
?>
<div class="input-field col s12">
	<select id="IDCurso2">
		<option value="" disabled selected>Elige el curso</option>
		<?php
		if(is_array($cursos)){
			foreach ($cursos as $key => $curso) {
			?>
			<option value='<?=$curso["ID"]?>'><?=$curso["Nombre"]?></option>
			<?php
			}
		}
		?>

		</select>
		<label>Elige el curso</label>
	</div>
<?php
}else{
?>
<form class="col s12">
			<div class="row">
				<div class="input-field col s12">
					<input placeholder="Nombre Clase" id="NombreClase" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input type="hidden" id="EID">
					<input placeholder="URL de video (Youtube)" id="URL" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<form class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<textarea id="DescripcionClase" class="materialize-textarea"></textarea>
							<label for="Descripcion">Textarea</label>
						</div>
					</div>
				</form>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<div id="NuevaClase" class="btn">Nueva Clase</div>
				</div>
			</div>
		</form>
<?php
}
?>
	
		
	</div>
</div>
<div id="editarClase" class="row cont">
	<div class="row">
		<form class="col s12">
			<div class="row">
				<div class="input-field col s12">
					<input placeholder="Nombre Clase" id="ENombreClase" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input type="hidden" id="EID">
					<input placeholder="URL de video (Youtube)" id="EURL" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<form class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<textarea id="EDescripcionClase" class="materialize-textarea"></textarea>
							<label for="Descripcion">Textarea</label>
						</div>
					</div>
				</form>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<div id="NuevaClase" class="btn">Nueva Clase</div>
				</div>
			</div>
		</form>
	</div>
</div>