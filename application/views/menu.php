<?php
    if(!$this->session->userdata('identificador')){
?>	
	<header>
		<div class="navbar-fixed">
			<nav>
			    <div class="nav-wrapper">
			    	<a href="<?=base_url()?>" class="brand-logo"><img src="<?=base_url('assets/img/logo-white.png')?>"></a>
			      	<a href="#" data-activates="mobile-demo" class="button-collapse">
			      		<i class="fa fa-bars" aria-hidden="true"></i>
			      	</a>
			      	<ul class="right hide-on-med-and-down">
			        	<li><a ref="#!" onclick="scrolls('.about-us-col')">Nosotros</a></li>
			        	<li><a href="#!" onclick="scrolls('.cursos-col')">Cursos</a></li>
			        	<li><a href="#!" onclick="scrolls('.registro-col')">Registro</a></li>
                <li><a href="#!" onclick="scrolls('.contacto-col')">Contacto</a></li>
			        	<li><a href="#!" id="login">Ingresar</a></li>
			      	</ul>
			      	<ul class="side-nav" id="mobile-demo">
			        	<li><a ref="#!" onclick="scrolls('.about-us-col')">Nosotros</a></li>
			        	<li><a ref="#!" onclick="scrolls('.cursos-col')">Cursos</a></li>
			        	<li><a href="#!" onclick="scrolls('.registro-col')">Registro</a></li>
                <li><a href="#!" onclick="scrolls('.contacto-col')">Contacto</a></li>
			        	<li><a href="#!" id="login">Ingresar</a></li>
			      	</ul>
			    </div>
		  	</nav>
	  	</div>
	</header>
<?php
}elseif($this->session->userdata('identificador')){
?>
<nav class="nav-fill">
    <div class="nav-wrapper">
      <a href="<?=base_url()?>" class="brand-logo"><img src="<?=base_url('assets/img/logo-white.png')?>"></a>
      <a href="#!" data-activates="mobile-demo" class="button-collapse">
        <i class="fa fa-bars" aria-hidden="true"></i>
      </a>
      <ul class="right hide-on-med-and-down">
        <li>
          	<form action="<?=base_url("inicio/buscar")?>" method="post">
          	<input name="parametro" type="search" placeholder="Buscar" class="input-nav search" required>
          	</form>
        </li>
        <?php
        if($this->session->userdata('perfil')==3){
        ?>
        <li><a href="<?=base_url('acceso')?>">Perfil</a></li>
        <li><a href="<?=base_url('proyectos')?>">Proyectos</a></li>
        <li><a href="<?=base_url('inicio/cursos')?>">Descubrir mas cursos</a></li>
        <?php
        }
        ?>
        <?php
        if($this->session->userdata('perfil')==1){
        ?>
        <li><a href="<?=base_url('acceso')?>">Panel</a></li>
        <?php
        }
        ?>
        <li><a href="<?=base_url('acceso/logout')?>">Salir</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <?php
        if($this->session->userdata('perfil')==3){
        ?>
        <li><a href="<?=base_url('acceso')?>">Perfil</a></li>
        <li><a href="<?=base_url('proyectos')?>">Proyectos</a></li>
        <li><a href="<?=base_url('inicio/cursos')?>">Descubrir mas cursos</a></li>
        <?php
        }
        ?>
        <?php
        if($this->session->userdata('perfil')==1){
        ?>
        <li><a href="<?=base_url('acceso')?>">Panel</a></li>
        <?php
        }
        ?>
        <li><a href="<?=base_url('acceso/logout')?>">Salir</a></li>
      </ul>
    </div>
</nav>
<?php
}
?>
