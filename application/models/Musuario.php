<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Musuario extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getUsuario($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('usuarios');
		return (($query->num_rows()>0)?$query->result_array():FALSE);
	}
	public function getInscripcion($id)
	{
		$this->db->where('IDUsuario', $id);
		$query = $this->db->get('inscripciones');
		return (($query->num_rows()>0)?$query->result_array():FALSE);
	}
	public function getProyectos($id)
	{
		$this->db->where('IDUsuario', $id);
		$query = $this->db->get('proyectos');
		return (($query->num_rows()>0)?$query->result_array():FALSE);
	}
	public function getCurso($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('cursos');
		return (($query->num_rows()>0)?$query->result_array():FALSE);
	}
	public function getGustos($id)
	{
		$this->db->where('IDUsuario', $id);
		$query = $this->db->get('gustosusuario');
		return (($query->num_rows()>0)?$query->result_array():FALSE);
	}
	public function getCategorias()
	{
		$query = $this->db->get('categorias');
		return (($query->num_rows()>0)?$query->result_array():FALSE);
	}
}

/* End of file Musuario.php */
/* Location: ./application/models/Musuario.php */