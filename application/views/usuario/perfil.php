	<?php
	$rands = array(
		'#F44336',
		'#E91E63',
		'#9C27B0',
		'#673AB7',
		'#3F51B5',
		'#2196F3',
		'#03A9F4',
		'#00BCD4',
		'#009688',
		'#4CAF50',
		'#8BC34A',
		'#CDDC39',
		'#FFEB3B',
		'#FFC107',
		'#FF9800',
		'#FF5722',
		'#795548',
		'#607D8B'
	);

	?>
	<div class="row perfil-titulo-row">
		<div class="col s4 m4 l4 push-l4 push-m4 push-s4 perfil-titulo-col">
			<h1>Perfil</h1>
		</div>
	</div>
	<div class="row parallax-row">
		<div class="col s12 m12 l12 parallax-col parallax-a3 perfil-parallax skew-top-cw">
			<div class="row perfil-row">
				<div class="col s12 m12 perfil-col">
					<div class="row perfil-tarjet-row">
						<div class="col s12 m12 l8 push-l2 perfil-tarjet-col">
							<div class="row perfil-avatar-row">
								<div class="col s4 m4 l4 push-l4 push-m4 push-s4 perfil-avatar-col">
									<img src="<?=base_url('assets/img/man-1.png')?>">
								</div>
							</div>
							<div class="row">
								<div class="col s12 m12 l12">
									<h1>Nombre</h1>
									<p><?=$usuario[0]['Nombres']." " .$usuario[0]['ApellidoPaterno']." ".$usuario[0]['ApellidoMaterno']?></p>
								</div>
							</div>
							<div class="row">
								<div class="col s12 m12 l12">
									<h1>Cursos Tomados</h1>
									<div class="row">
									<?php
									if(isset($cursos)){
										$j=1;
										for ($i=0;$i<count($cursos);$i++) { 
                    					foreach($cursos[$i] as $value){
                    					echo '
                    					<a href="'.base_url('inicio/curso/'.$value['ID'].'').'">
										<div class="col s12 m12 l3 curso-picture" style="background:'.$rands[rand(0,17)].';background-size: 100% 100%;">
											<div class="label-picture">
												<p>'.$value['Nombre'].'</p>
											</div>
										</div></a>';
										if($j<4){
                            			$j++;
                        				}else{
                            				echo'</div><div class="row">';
                            				$j=1;
                        				}
                        				}
										}
									}else{
										echo '<h4>No ha tomado ningun curso</h4>';
									}
									?>
									</div>
								</div>
							</div>
							<div class="row perfil-proyectos-row">
								<div class="col s12 m12 l12">
									<h1>Proyectos Realizados</h1>
									<div class="row">
									<?php
									if(isset($proyectos)){
										$i=1;
                    					foreach($proyectos as $key => $value){
                    					echo '
										<div class="col s12 m12 l3 curso-picture" style="background:'.$rands[rand(0,17)].';background-size: 100% 100%;">
											<div class="label-picture">
												<p>'.$value['Nombre'].'</p>
											</div>
										</div>';
										if($i<4){
                            			$i++;
                        				}else{
                            				echo'</div><div class="row">';
                            				$i=1;
                        				}
										}
									}else{
										echo '<h4>No ha tomado ningun curso</h4>';
									}
									?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>