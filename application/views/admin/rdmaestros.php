<div class="wrapper">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="row table-wrapper-row">
                <div class="col s12 m12 l12 table-wrapper-col">
                    <table id="maestrosTabla" class="responsive-table tabla-dashboard">
                        <thead>
                            <tr>
                                <td><input type="text" placeholder="Búsqueda ID" /></td>
                                <td><input type="text" placeholder="Nombres" /></td>
                                <td><input type="text" placeholder="Apellido Paterno" /></td>
                                <td><input type="text" placeholder="Apellido Materno" /></td>
                                <td><input type="text" placeholder="Correo" /></td>
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Nombres</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Correo</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>