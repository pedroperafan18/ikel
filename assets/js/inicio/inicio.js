$(document).ready(function(){  
	$("#inscribir").click(function{
		var curso = $(this).data("curso");
		$.ajax({
			url: url+'inicio/inscribir',
			type: 'POST',
			data: {Curso:curso},
		}).always(function(data) {
			if(data.result!=false){
				window.location=base_url+"inicio";
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
			console.log("complete");
		});
	});

	$("#contacto").click(function{
		var correo = $("#correo_contacto").val();
		$.ajax({
			url: url+'inicio/contacto',
			type: 'POST',
			data: {Tipo:tipo},
		}).always(function(data) {
			if(data.result!=false){
				window.location=base_url+"inicio";
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
			console.log("complete");
		});
	});

	$("#vermas").click(function{
		var segments = $(this).data("url");
		$.ajax({
			url: url+'inicio/verMas/'+segments+'/',
			type: 'GET',
		}).always(function(data) {
			if(data!=false){
				$(".videos").html("");
				$(".videos").append("");
				$("#vermas").data("url",segments+1);
				$("#cursos").append(data.contenido);
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
			console.log("complete");
		});
	});

	$("#curso").click(function{
		var curso = $(this).data("curso");
		$.ajax({
			url: url+'inicio/curso/'+curso,
			type: 'POST',
			data: {Curso:curso},
		}).always(function(data) {
			if(data.result!=false){
				
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
			console.log("complete");
		});
	});

	$("#comentar").clcik(function{
		var segments = url.split( '/' );
		var clase = segments[4];
		var comentario = $("#comentario").val();
		$.ajax({
			url: url+'inicio/comentar',
			type: 'POST',
			data: {IDClase:clase,Comentario:comentario},
		}).always(function(data) {
			if(data.result!=false){
			}else{
				$.each(data.error, function(index, val) {
					if (typeof val != 'undefined') {
						Materialize.toast(val, 12000, 'rounded');
					}
				});
			}
			console.log("complete");
		});
	});
});