<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MProyectos extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function proyectos(){
		$this->db->where("Habilitado",1);
		$this->db->limit(20);
		$this->db->order_by("Fecha","DESC");
		$query=$this->db->get("proyectos");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function contenidoproyectos($id){
		$this->db->where("Habilitado",1);
		$this->db->where("IDProyecto",$id);
		$this->db->limit(20);
		$this->db->order_by("ID","DESC");
		$query=$this->db->get("contenidoproyectos");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function proyecto($id){
		$this->db->where("ID",$id);
		$query=$this->db->get("proyectos");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function contenidoProyecto($id){
		$this->db->where("ID",$id);
		$query=$this->db->get("contenidoproyectos");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function ncontenidoProyecto($data){
		if($this->db->insert("contenidoproyectos",$data)){
			return insert_id();
		}else{
			return FALSE;
		}
	}
	public function crearProyecto($datos){
		$data = array('Nombre' => $datos["Nombre"],
						'IDUsuario' => $datos["ID"],
						'Descripcion' => $datos["Descripcion"]);
		if($this->db->insert("proyectos",$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
	public function acontenidoProyecto($data,$id){
		$this->db->where("IDProyecto",$id);
		if($this->db->update("contenidoproyectos",$data)){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function actualizarProyecto($datos,$id){
		$data = array('Nombre' => $datos["Nombre"],
						'IDUsuario' => $datos["ID"],
						'Descripcion' => $datos["Descripcion"],
						'Fecha' => $datos["Fecha"]);
		$this->db->where("ID",$id);
		if($this->db->update("proyectos",$data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function eliminarProyecto($id){
		$data = array('Habilitado' => 0);
		$this->db->where("ID",$id);
		if($this->db->update("proyectos",$data)&&$this->db->update("contenidoproyectos",$data)){
			return TRUE;
		}else{
			return FALSE;
		}

	}

	public function comentarProyecto($idp,$id,$datos){
		$data = array('IDProyecto' => $idp,
					'IDUsuario' => $id,
					'Comentario' => $datos["Comentario"]);
		if($this->db->insert("retroalimentacion",$data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function comentarios($id){
		$this->db->where("IDProyecto",$id);
		$query = $this->db->get("retroalimentacion");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;	
		}
	}

	public function uComentarios($id){
		$this->db->where("ID",$id);
		$query = $this->db->get("usuarios");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;	
		}
	}
}