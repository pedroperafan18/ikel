<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('form_validation', 'session','user_agent'));
        $this->load->model('minicio');
        $this->CI = & get_instance();
    }

    public function index() {
        $data_head["titulo"]="Bienvenido | IKEL";
        $data_head["descripcion"]="Todos tienen algo que aportar.";
        $data_head["keywords"]="";
        $data_head["ico"]=base_url("assets/img/ico.png");
		$data_head["css"]=array("materialize.min","font-awesome.min","paladium.min");
        $data_javascript["script"]=array("jquery","materialize.min","general.min","inicio/inicio.min","inicio/acceso.min");
      	$data["head"]       = $this->load->view("head",$data_head,TRUE);  
        $data["javascript"] = $this->load->view("javascript",$data_javascript,TRUE);
        $data["menu"]       = $this->load->view("menu","",TRUE);
        $data["footer"]     = $this->load->view("footer","",TRUE);
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->session->userdata('perfil') == 1){
                $id = $this->session->userdata('identificador');
                
                $data_contenido["solicitudes"] = $this->minicio->getSolicitudes($id);
                $data_contenido["mascursos"] = $this->minicio->masCursos($id);
                $data["contenido"]  = $this->load->view("inicio/dashboard",$data_contenido,TRUE); 
            }
            else{
                redirect('usuario/perfil','refresh');
            }
        }else{

            $data["contenido"] = $this->load->view('inicio/inicio',NULL, TRUE);

        }
        $this->load->view("html",$data);
        // Fin vista index.
            
    }

    function logData(){
        
        date_default_timezone_set("America/Mexico_City");

        $times = $this->CI->db->query_times; 
        foreach ($this->CI->db->queries as $key => $query) { 
            if (!preg_match('/SELECT/', $query)) {
                $query = str_replace("INTO ", "", $query);
                $sql = explode(' ',$query);
                $data["IDUsuario"] = $this->session->userdata('identificador');
                $data["Accion"] = $sql[0];
                $data["Tabla"] = str_replace("`", "", $sql[1]);
                $data["IP"] = $this->input->ip_address();
                $patron = "/([0-9]+)/";
                if (preg_match($patron, $query,$id)) {
                    $data["IDElemento"] = $id[1];
                }else{
                    $data["IDElemento"] = 0;
                }                
                if ($this->agent->is_browser())
                {
                    $data["Navegador"] = $this->agent->browser().' '.$this->agent->version();
                }
                elseif ($this->agent->is_robot())
                {
                    $data["Navegador"] = $this->agent->robot();
                }
                elseif ($this->agent->is_mobile())
                {
                    $data["Navegador"] = $this->agent->mobile();
                }
                else
                {
                    $data["Navegador"] = 'OS sin identifiar';
                }
                $data["SistemaOperativo"] = $this->agent->platform();
                $data['Fecha'] = date('Y-m-d H:i:s');
                $this->minicio->setLog($data);
            }
                
        }

    }

    public function verMas() {
    	if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
    		if($this->uri->segment(3)){
    			$numero = $this->uri->segment(3);
    			if(is_numeric($numero))
    			{
    				$ultimoCurso = $numero * 4;
    				if($this->minicio->masCursos($ultimoCurso)){
    					$data['cursos'] = $this->minicio->masCursos($ultimoCurso);
    				}
    				if(isset($data)){
    					$data['contenido'] = $this->load->view('inicio/videos', $data, TRUE);
    				}
    				else{
    					$data['contenido'] = $this->load->view('inicio/videos', NULL, FALSE);
    				}
                    $this->output->set_content_type('application/json')->set_output(json_encode($data));
                }
    			else{
    				// No es numerico.
    				//redirect('inicio/verMas/1','refresh');
                    $data = array("result"=>false,"error"=>"Error.");
                     $this->output->set_content_type('application/json')->set_output(json_encode($data));
    			}
    		}
    		else{
    			//redirect('inicio/verMas/1','refresh');
                 $this->output->set_content_type('application/json')->set_output(json_encode($data));
    		}
    	}
    	else{
    		// No hay sesion, redirigir a index.
    		//redirect('inicio/inicio','refresh');
             $this->output->set_content_type('application/json')->set_output(json_encode($data));
    	}    	
    }

    public function video() {
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){

    		if($this->uri->segment(3)){
    			$video = $this->uri->segment(3);
    			if(is_numeric($video)){
    				$result = $this->minicio->getVideo($video);
                    if($result != FALSE){
                        $data_header["titulo"] = "IKEL";
                        $data_head["descripcion"]="Todos tienen algo que aportar.";
                        $data_head["keywords"]="";
                        $data_header["imagen"] = "";
                        $data_header["cssExterno"] = array();
                        $data_header["ico"]=base_url("assets/img/ico.png");
                        $data_header["css"]=array("materialize.min","font-awesome.min","paladium.min");
                        $data_javascript["script"]=array("jquery","materialize.min","general.min");
                        $data_javascript["scriptExterno"] = array();
                        $data_extras["contenido"] = ""; //Puede ser un array
                        
                        $data["contenido"] = $this->load->view("inicio/video", $result["0"] , TRUE);
                        $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
                        $data["head"] = $this->load->view("head", $data_header, TRUE);
                        $data["menu"] = $this->load->view("menu", NULL , TRUE);
                        $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
                        $data["footer"] = $this->load->view("footer", NULL , TRUE);
                        $this->load->view("html",$data);
                    }
                    else{
                        redirect('inicio','refresh');
                    }
                }
                else{
                    // No es numerico.
                    redirect('inicio/video','refresh');
                }
            }
            else{
                // Error al traer el video.
                redirect('inicio/video','refresh');
            }
        }
        else{
            // No existe sesion.
            redirect('inicio/inicio','refresh');
        }
    }

    public function curso() {
    	if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
    		if($this->uri->segment(3)){
                $curso = $this->uri->segment(3);
                if(is_numeric($curso)){
                    $data_curso["curso"] = $this->minicio->getCurso($curso);

                    if( $data_curso["curso"] != FALSE){
                       $data_header["titulo"] = "IKEL";
                        $data_head["descripcion"]="Todos tienen algo que aportar.";
                        $data_head["keywords"]="";
                        $data_header["imagen"] = "";
                        $data_header["cssExterno"] = array();
                        $data_header["ico"]=base_url("assets/img/ico.png");
                        $data_header["css"]=array("materialize.min","font-awesome.min","paladium.min");
                        $data_javascript["script"]=array("jquery","materialize.min","general.min");
                        $data_javascript["scriptExterno"] = array();
                        $data_extras["contenido"] = ""; //Puede ser un array

                        $data_curso["videos"] = $this->minicio->getContenidoCursos($curso);
                         //   $data_curso["cursos"] =$this->minicio->getVideosCurso($data_curso["curso"]["0"]["ID"]);
                        
                        $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
                        $data["head"] = $this->load->view("head", $data_header, TRUE);
                        $data["menu"] = $this->load->view("menu", NULL , TRUE);
                        $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
                        $data["footer"] = $this->load->view("footer", NULL , TRUE);
                        $data["contenido"] = $this->load->view("inicio/cursos", $data_curso , TRUE);
                        $this->load->view("html",$data);
                    }
                    else{
                        //redirect('inicio','refresh');
                        echo "4";
                    }
                }
                else{
                    // No es numerico.
                    //redirect('inicio','refresh');
                    echo "3";
                }
    		}else{
    			// Error al traer el curso.
    			//redirect('inicio','refresh');
                echo "2";
    		}	
        }
        else{
            // No existe sesion, redirigir a index.
            //redirect('inicio','refresh');
            echo "1";
        }
    }
    public function cursos(){
        $data_header["titulo"] = "cursos";
        $data_header["descripcion"] = "";
        $data_header["imagen"] = "";
        $data_header["ico"]=base_url("assets/img/ico.png");
                        $data_header["css"]=array("materialize.min","font-awesome.min","paladium.min");
                        $data_javascript["script"]=array("jquery","materialize.min","general.min"); 
        $data_javascript["scriptExterno"] = array();
        $data_extras["contenido"] = ""; //Puede ser un array
        $id = $this->session->userdata('identificador');
                
                $data_contenido["solicitudes"] = $this->minicio->getSolicitudes($id);
                $data_contenido["mascursos"] = $this->minicio->masCursos($id);
                $data["contenido"]  = $this->load->view("inicio/dashboard",$data_contenido,TRUE); 
        $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
        $data["head"] = $this->load->view("head", $data_header, TRUE);
        $data["menu"] = $this->load->view("menu", NULL , TRUE);
        $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
        $data["footer"] = $this->load->view("footer", NULL , TRUE);
        $this->load->view("html",$data);
    }    
    public function buscar(){
        $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
        if($this->input->post()!=NULL){
            foreach ($data as $key => $value) {
                $data["$key"] = $this->security->xss_clean($value);
            }
            if (isset($data['parametro'])) {
                $parametro = $data['parametro'];
                $data_contenido['resultados'] = $this->minicio->getResultados($parametro);
            }
            $data_header["titulo"] = "IKEL";
            $data_head["descripcion"]="Todos tienen algo que aportar.";
            $data_head["keywords"]="";
            $data_header["imagen"] = "";
            $data_header["cssExterno"] = array();
            $data_header["ico"]=base_url("assets/img/ico.png");
            $data_header["css"]=array("materialize.min","font-awesome.min","paladium.min");
            $data_javascript["script"]=array("jquery","materialize.min","general.min");
            $data_javascript["scriptExterno"] = array();
            $data_extras["contenido"] = ""; //Puede ser un array
            $data["contenido"] = $this->load->view("buscador/buscador", $data_contenido, TRUE);
            $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
            $data["head"] = $this->load->view("head", $data_header, TRUE);
            $data["menu"] = $this->load->view("menu", NULL , TRUE);
            $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
            $data["footer"] = $this->load->view("footer", NULL , TRUE);
            $this->load->view("html",$data);
        }else{
            $data_header["titulo"] = "IKEL";
            $data_head["descripcion"]="Todos tienen algo que aportar.";
            $data_head["keywords"]="";
            $data_header["imagen"] = "";
            $data_header["cssExterno"] = array();
            $data_header["ico"]=base_url("assets/img/ico.png");
            $data_header["css"]=array("materialize.min","font-awesome.min","paladium.min");
            $data_javascript["script"]=array("jquery","materialize.min","general.min");
            $data_javascript["scriptExterno"] = array();
            $data_extras["contenido"] = ""; //Puede ser un array
            $data["contenido"] = $this->load->view("buscador/buscador", NULL , TRUE);
            $data["extras"] = $this->load->view("extras", $data_extras, TRUE);
            $data["head"] = $this->load->view("head", $data_header, TRUE);
            $data["menu"] = $this->load->view("menu", NULL , TRUE);
            $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
            $data["footer"] = $this->load->view("footer", NULL , TRUE);
            $this->load->view("html",$data);
        }
    }

    public function suscribirse(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = $this->input->post();
                if($this->input->post()!=NULL){
                    
                    foreach ($data as $key => $value) {
                        $data["$key"] = $this->security->xss_clean($value);
                    }
                    $this->form_validation->set_rules('ID','ID','is_unique[inscripciones.IDUsuario]');

                    if ($this->form_validation->run() != FALSE) {
                        if (isset($data['ID']) && !empty($data['ID'])) {
                            $data['IDUsuario'] = $this->session->userdata('identificador');
                            $data['IDCurso'] = $data['ID'];
                            unset($data['ID']);

                            $result = $this->minicio->setSuscripcion($data);       
                        }
      
                        if ($result != false) {
                            $array = array('result' => TRUE);
                            $this->logData();
                        }else{
                        //Hubo error al insertar/actualizar la categoria
                            $array = array('result' => FALSE);
                        }
                    }else{
                        //Error al validar los datos
                        $errors = $this->form_validation->error_array();
                        $array = array("result" => FALSE,"error" => $errors);
                    }

                    $this->output->set_content_type("application/json")->set_output(json_encode($array));
                }
            }
        }else{
            redirect("acceso/login","refresh");
        }
    }

}