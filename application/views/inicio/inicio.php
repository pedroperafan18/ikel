	<!--modal -->
	<div class="modal-ingreso">
		<div class="container">
			<div class="row modal-ingreso-row">
				<div class="col s12 m12 l6 push-l3 modal-ingreso-col">
					<div class="row modal-header-row">
						<div class="col s12 m12 l12 modal-header-col">
							<div class="row">
								<img class="col s12 m12 l4 push-l4" src="<?=base_url('assets/img/inicio/avatar.png')?>">
							</div>
							<div class="row">
								<div class="col s12 m12 l12">
									<h1>Ingresar</h1>
									<p>Ingresa tus datos para poder acceder</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<form class="col s12 m12 l12">
							<div class="container">
		        				<div class="input-field col s12 m12 l12">
		          					<input id="correo" type="email" class="validate" required>
		          					<label for="correo">Correo Electrónico</label>
		        				</div>
		        				<div class="input-field col s12 m12 l12">
		          					<input id="password" type="password" class="validate" required="">
		          					<label for="password">Contraseña</label>
		        				</div>
		        				<div class="row">
			        				<div class="col s12 m12 l12 submit-ingreso">
				        				<button type="button" class="bttn bttn-submit" id="ingreso">Ingresar</button>
			        				</div>
		        				</div>
		        			</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<!--modal-->
	
	<div class="wrapper wrapper-index">
		<div class="slider">
			<ul class="slides">
				<li>
				    <img src="<?=base_url('assets/img/inicio/slider-1.jpg')?>">
				    <div class="caption center-align">
				        <h3>Capacítate</h3>
				        <h5 class="light grey-text text-lighten-3">Obtén una oportunidad de aprender de manera gratuita</h5>
				    </div>
				</li>
				<li>
				   	<img src="<?=base_url('assets/img/inicio/slider-2.jpg')?>">
				    <div class="caption left-align">
				        <h3>Colabora</h3>
				        <h5 class="light grey-text text-lighten-3">Se parte de una sociedad colaborativa aportando tus conocimientos</h5>
				    </div>
				</li>
				<li>
				    <img src="<?=base_url('assets/img/inicio/slider-3.jpg')?>">
				    <div class="caption right-align">
				        <h3>Demuestra</h3>
				        <h5 class="light grey-text text-lighten-3">Presenta el producto de tu habilidad adquiridas</h5>
				    </div>
				</li>
				<li>
				    <img src="<?=base_url('assets/img/inicio/slider-4.jpg')?>">
				    <div class="caption center-align">
				   		<h3>Actualiza</h3>
				        <h5 class="light grey-text text-lighten-3">Aumenta tus habilidades conociendo temas nuevos</h5>
				    </div>
				</li>
			</ul>
			<img src="<?=base_url('assets/img/inicio/scroll.png')?>" class="arrow-slider hide-on-small-only">
		</div>

		<div class="row about-us-row">
			<div class="col s12 m12 l12 about-us-col skew-bottom-ccw">
				<div class="row about-us-content-row">
					<div class="col s12 m12 l8 push-l2 about-us-content-col">
						<div class="row">
							<div class="col s8 m8 l8">
								<h1>¿Quienes somos?</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod quam eu molestie dictum. Donec vel interdum turpis, ac rutrum leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin a purus consectetur, pretium ligula in, iaculis eros. In eget molestie lacus, non mattis ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
							</div>
							<div class="col s4 m4 l4">
								<img src="<?=base_url('assets/img/inicio/coworking.png')?>">
							</div>
						</div>
				</div>
			</div>
		</div>

		<div class="row parallax-row">
			<div class="col s12 m12 l12 parallax-col parallax-a1 skew-top-cw skew-bottom-cw">
			</div>
		</div>

		<div class="row flyer-row">
			<div class="col s12 m12 l12 flyer-col">
				<div class="row">
					<div class="col s12 m12 l6 push-l3">
						<img src="<?=base_url('assets/img/flyer_ikel.png')?>">
					</div>
				</div>
			</div>
		</div>

		<div class="row cursos-row">
			<div class="col s12 m12 l12 cursos-col skew-top-ccw skew-bottom-ccw">
				<div class="row cursos-content-row">
					<div class="col s12 m12 l8 push-l2 cursos-content-col">
						<h1>Algunas categorías</h1>
						<div class="row">
							<div class="col s3 m3 l3 cursos-curso-col">
								<img src="<?=base_url('assets/img/inicio/briefcase.png')?>">
								<label>Oficios</label>
							</div>
							<div class="col s3 m3 l3 cursos-curso-col">
								<img src="<?=base_url('assets/img/inicio/light-bulb.png')?>">
								<label>Habilidades</label>
							</div>
							<div class="col s3 m3 l3 cursos-curso-col">
								<img src="<?=base_url('assets/img/inicio/photo-camera.png')?>">
								<label>Fotografía</label>
							</div>
							<div class="col s3 m3 l3 cursos-curso-col">
								<img src="<?=base_url('assets/img/inicio/contract.png')?>">
								<label>Finanzas</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row descripcion-row">
			<div class="col s12 m12 l12 descripcion-col">
				<div class="row descripcion-texto-row">
					<div class="col s12 m12 l8 push-l2 descripcion-texto-col">
						<div class="container">
							<h1>¿Que logramos?</h1>
							<p>
								<i class="fa fa-quote-left" aria-hidden="true"></i>
								Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.
								<i class="fa fa-quote-right" aria-hidden="true"></i>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row parallax-row registro-row">
			<div class="col s12 m12 l12 parallax-col parallax-a2 registro-col skew-top-cw skew-bottom-ccw">
				<div class="row registro-box-row">
					<div class="col s12 m12 l12 registro-box-col">
						<h1>Registro</h1>
						<div class="row registro-form-row">
							<form class="col s10 m10 l6 push-l3 push-s1 push-m1 registro-form-col" id="registro_datos">
								<div class="container">
									<p>Para acceder a los cursos, ingresa tu informacion para crear una cuenta</p>
									<div class="input-field col s12 m12 l12">
		          						<input id="Nombre" type="text" class="validate" required>
		          						<label for="Nombre">Nombre</label>
		        					</div>

		        					<div class="input-field col s12 m12 l6">
		          						<input id="Paterno" type="text" class="validate" required>
		          						<label for="Paterno">Apellido Paterno</label>
		        					</div>

		        					<div class="input-field col s12 m12 l6">
		          						<input id="Materno" type="text" class="validate" required>
		          						<label for="Materno">Apellido Materno</label>
		        					</div>		

		        					<div class="input-field col s12 m12 l12">
		          						<input id="Correo" type="email" class="validate" required>
		          						<label for="Correo">Correo Electrónico</label>
		        					</div>

		        					<div class="input-field col s12 m12 l12">
		          						<input id="Password" type="password" class="validate" required="">
		          						<label for="Password">Contraseña</label>
		        					</div>
		        					<div class="bttn bttn-submit" id="registro">Registrar</div>
		        				</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row descripcion-row">
			<div class="col s12 m12 l12 descripcion-col">
				<div class="row descripcion-texto-row">
					<div class="col s12 m12 l8 push-l2 descripcion-texto-col">
						<div class="container">
							<h1>¿Deseas ayudar siendo un docente?</h1>
							<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor.</p>
						</div>
					</div>
				</div>
				
			</div>
		</div>

		<div class="row contacto-row">
			<div class="col s12 m12 l12 contacto-col skew-top-cw">
				<div class="container">
				<div class="row contacto-wrapper-row">
					<div class="col s12 m12 l8 push-l2 contacto-wrapper-col">
						<h1>Contacto</h1>
						<h4>¿Deseas conocer mas?, proporciona tu correo y recibiras información</h4>
						<div class="row">
							<form class="col s12 m12 l12">
								<input class="input-contacto" placeholder="Correo Electonico" id="correo_contacto">
								<button type="button" class="bttn bttn-contacto" id="contacto">Enviar</button>
							</form>
						</div>

					</div>
				</div>	
				</div>
			</div>
		</div>
	</div>