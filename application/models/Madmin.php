<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Madmin extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function login($data){
		$this->db->where('Correo',$data['Correo']);
		$this->db->where('Password',$data['Password']);
		$query = $this->db->get('usuario');
		if ($query->num_rows() > 0){
          return $query->result_array();
        } else {
          return FALSE;
        }
	}

	public function setLog($data)
	{
		$this->db->insert('log',$data);
	}

	/*CRUD CATEGORIAS*/
	public function getCategoria($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('Categorias');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editarCategoria($id,$data)
	{
		$this->db->where('Habilitado', "1");
		$this->db->where('ID', $id);
		if($this->db->update('', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarCategoria($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('Categorias', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevoCategoria($data)
	{
		if($this->db->insert('Categorias',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
	}
	/*CRUD ADMINISTRADOR*/
	public function getAdministrador($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('Usuarios');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function editarAdministrador($id,$data)
	{
		$this->db->where('Habilitado', "1");
		$this->db->where('ID', $id);
		if($this->db->update('', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function eliminarAdministrador($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('Usuarios', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function nuevoAdministrador($data)
	{
		if($this->db->insert('Usuarios',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
	}

	/*DR CURSOS*/
	public function getCurso($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('Cursos');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function eliminarCurso($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('Cursos', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*DR MAESTROS*/
	public function getMaestro($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('Maestros');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function eliminarMaestro($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('Maestros', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	/*DR TEMAS*/
	public function getTema($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('Temas');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function eliminarTema($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('Temas', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	/*DR CLASES*/
	public function getClase($id)
	{
		$this->db->where('ID', $id);
		$this->db->where('Habilitado', "1");
		$query = $this->db->get('Clases');
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function eliminarClase($id)
	{
		$this->db->where('ID', $id);
		$data = array('Habilitado' => '0');
		if($this->db->update('Clases', $data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
}