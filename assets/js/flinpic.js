$.postFlinpic = function(ruta,parametros,funcion=null,error=false){
	parametros.token = token;
	$.post(base_url+ruta,parametros,funcion).always(function(data){
		if(error=true){
			if(typeof data.responseText != "undefined"){
				console.log(data.responseText);
			}
		}
	});
}

$.getFlinpic = function(ruta,funcion=null,error=false){
	$.get(base_url+ruta+"&token="+token,funcion).always(function(data){
		if(error=true){
			if(typeof data.responseText != "undefined"){
				console.log(data.responseText);
			}
		}
	});
}

$.ajaxPlus = function(ruta,type,parametros,funcion=null,error=false){
	parametros.token = token;
	$.ajax({
		url: base_url+ruta,
		type: type,
		data: parametros,
		contentType: false,
        processData: false,
	}).done(funcion).fail(function(data) {
		if(error=true){
			if(typeof data.responseText != "undefined"){
				console.log(data.responseText);
			}
		}
	});
}

