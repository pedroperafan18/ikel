<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proyectos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('session','form_validation','user_agent','upload'));
        $this->load->helper('url', 'security');
        $this->load->model('mproyectos');
	}

	public function index()
	{   
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            $proyectos = $this->mproyectos->proyectos();
            $data_contenido["uno"] = 1;
            if(is_array($proyectos)){
                foreach ($proyectos as $key => $value) {
                    $cproyectos[$key] = $this->mproyectos->contenidoproyectos($value["ID"]);
                }
                $data_contenido["proyectos"] = $proyectos;
                $data_contenido["cproyectos"] = $cproyectos;
                unset($data_contenido["uno"]);
            }
            
            $data_header["titulo"] = "Proyectos";
            $data_header["descripcion"] = "Proyectos";
            $data_header["imagen"] = "assets/js/img/ico.png";
            $data_header["css"] = array('font-awesome.min','materialize.min','paladium.min');
            $data_javascript["script"] = array('jquery','materialize.min','proyectos/proyectos','general');
            $data["contenido"] = $this->load->view("proyectos/proyectos", $data_contenido, TRUE);
            $data["head"] = $this->load->view("head", $data_header, TRUE);
            $data["menu"] = $this->load->view("menu", NULL , TRUE);
            $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
            $data["footer"] = $this->load->view("footer", NULL , TRUE);
            $this->load->view("html",$data);
        }else{
          redirect("inicio","refresh");
        }
	}

    public function proyecto(){
        //if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            $id=$this->uri->segment(3);
            if($this->mproyectos->proyecto($id)!=FALSE){
                $data_contenido["proyecto"] = $this->mproyectos->proyecto($id);
                $data_contenido["proyectoimg"] = $this->mproyectos->contenidoProyecto($id);
                $comentarios = $this->mproyectos->comentarios($id);
                $data_contenido["comentarios"] = $this->mproyectos->comentarios($id);
                if(is_array($comentarios)){
                    foreach ($comentarios as $key => $value) {
                        $data_contenido["usuarios"] = $this->mproyectos->uComentarios($value["IDUsuario"]);
                    }
                }
            }else{
                $data_contenido["proyecto"] = "";
                $data_contenido["proyectoimg"] ="";
                $data_contenido["comentarios"] = "";
                $data_contenido["usuarios"] = "";
            }
            $data_header["titulo"] = "Proyectos";
            $data_header["descripcion"] = "Proyectos";
            $data_header["imagen"] = "assets/js/img/ico.png";
            $data_header["css"] = array('font-awesome.min','materialize.min','paladium.min');
            $data_javascript["script"] = array('jquery','materialize.min',"usuario/proyectos",'general');
            $data["contenido"] = $this->load->view("proyectos/proyecto", $data_contenido, TRUE);
            $data["head"] = $this->load->view("head", $data_header, TRUE);
            $data["menu"] = $this->load->view("menu", NULL , TRUE);
            $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
            $data["footer"] = $this->load->view("footer", NULL , TRUE);
            $this->load->view("html",$data);
        /*}else{
          redirect("inicio","refresh");
        }*/
    }

    public function crearProyecto(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
            if($this->input->post()!=NULL){
                //Inicio AJAX/POST
                foreach ($data as $key => $value) {
                    $data["$key"] = $this->security->xss_clean($value);
                }
                $this->form_validation->set_rules('Nombre','Nombre', 'trim|required|max_length[150]');
                $this->form_validation->set_rules('Descripcion','Descripcion', 'trim|required|max_length[500]');
                    if($this->form_validation->run() == FALSE){
                        $errors = $this->form_validation->error_array();
                        $array = array('result' => FALSE, "error" => $errors);
                    } else{
                    $data["ID"] = 1;
                    if($this->uri->segment(3)!=NULL){
                        $idp = $this->uri->segment(3);
                        $this->mproyectos->actualizarProyecto($data,$idp);
                            $data = array("IDProyecto" => $idp,
                                            "Contenido" => $data["Contenido"],
                                            "tipo"=> 2,
                                            "Habilitado" => 1);
                            if($this->mproyectos->aContenidoProyecto($data,$idp)==TRUE){
                                $array = array('result' => TRUE);
                            }else{
                                $array = array('result' => FALSE);
                            }
                    }else{
                        $idp=$this->mproyectos->crearProyecto($data);
                            $datos = array("IDProyecto" => $idp,
                                            "Contenido" => $data["Contenido"],
                                            "Tipo"=> 2,
                                            "Habilitado" => 1);
                                
                                $array = array('result' => $this->mproyectos->nContenidoProyecto($datos));
                            $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }
                }
            }
            }else{
                $data_header["titulo"] = "Proyectos";
                $data_header["descripcion"] = "Proyectos";
                $data_header["imagen"] = "assets/js/img/ico.png";
                $data_header["css"] = array('font-awesome.min','materialize.min','paladium.min');
                $data_javascript["script"] = array('jquery','materialize.min',"usuario/proyectos",'general');
                $data["contenido"] = $this->load->view("proyectos/crearproyecto", NULL, TRUE);
                $data["head"] = $this->load->view("head", $data_header, TRUE);
                $data["menu"] = $this->load->view("menu", NULL , TRUE);
                $data["javascript"] = $this->load->view("javascript", $data_javascript , TRUE);
                $data["footer"] = $this->load->view("footer", NULL , TRUE);
                $this->load->view("html",$data);
            }
        }else{
           redirect("inicio","refresh");
        }
    }

    public function eliminarProyecto(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
                if($this->input->post()!=NULL){
                    $id = $this->uri->segment(3);
                    if($this->mproyectos->eliminarProyecto($id)==TRUE){
                        $array = array('result' => TRUE);
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }else{
                         $array = array('result' => FALSE);
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }   
                }
            }else{
                redirect("inicio","refresh");
            }
        }
    }

    public function comentar(){
        if($this->session->userdata('identificador') && $this->session->userdata('perfil')){
            if($this->input->is_ajax_request()){
                $data = ($this->input->post()==NULL)? $this->input->get() : $this->input->post();
                if($this->input->post()!=NULL){
                    $id = $this->session->userdata('identificador');
                    $idp = $this->uri->segment(3);
                    foreach ($data as $key => $value) 
                    {
                        $data["$key"] = $this->security->xss_clean($value);
                    }
                    $this->form_validation->set_rules('Comentario', 'Comentario', 'trim|required|max_length[500]');
                    if($this->form_validation->run() == FALSE){
                        $errors = $this->form_validation->error_array();
                        $array = array('result' => FALSE, "error" => $errors);
                    } else{
                          if($this->mproyectos->comentarProyecto($idp,$id,$data)){
                        $array = array('result' => TRUE);
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }else{
                         $array = array('result' => FALSE);
                        $this->output->set_content_type("application/json")->set_output(json_encode($array));
                    }   
                    }
                }
            }
        }else{
            redirect("inicio","refresh");
        }

    }        
}
