<div id="verCursos" class="row cont">
	<table>
		<thead>
			<tr>
				<th data-field="id">IDCurso</th>
				<th data-field="nombre">Nombre</th>
				<th data-field="editar"></th>
				<th data-field="eliminar"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		if(is_array($cursos)){
			foreach ($cursos as $key => $curso) {
			?>
			<tr data-curso="<?=$curso["ID"]?>">
				<td><?=$curso["ID"]?></td>
				<td><?=$curso["Nombre"]?></td>
				<td><div class="btn blue editarCurso" data-action="editarCurso" data-id="<?=$curso["ID"]?>">EDITAR</div></td>
				<td><div class="btn red eliminarCurso" data-id="<?=$curso["ID"]?>">ELIMINAR</div></td>
			</tr>
			<?php
			}
		}
		?>
			
		</tbody>
	</table>
</div>

<div id="CrearCursos" class="row cont">
	<div class="row">
		<form class="col s12">
			<div class="row">
				<div class="input-field col s12">
					<input type="hidden" id="EID">
					<input placeholder="Nombre Curso" id="Nombre" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<select id="Categoria">
						<option value="" disabled selected>Categoria</option>
						<?php
						foreach ($categorias as $key => $categoria) {
							echo "<option value='".$categoria["ID"]."'>".$categoria["Nombre"]."</option>";
						}
						?>
					</select>
					<label>Categoria</label>
				</div>
			</div>
			<div class="row">
				<form class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<textarea id="Descripcion" class="materialize-textarea"></textarea>
							<label for="Descripcion">Textarea</label>
						</div>
					</div>
				</form>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<div id="NuevoCurso" class="btn">Nuevo Curso</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="editarCurso" class="row cont">
	<div class="row">
		<form class="col s12">
			<div class="row">
				<div class="input-field col s12">
					<input placeholder="Nombre Curso" id="ENombre" type="text" class="validate">
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<select id="ECategoria">
						<option value="" disabled selected>Categoria</option>
						<?php
						foreach ($categorias as $key => $categoria) {
							echo "<option value='".$categoria["ID"]."'>".$categoria["Nombre"]."</option>";
						}
						?>
					</select>
					<label>Categoria</label>
				</div>
			</div>
			<div class="row">
				<form class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<textarea id="EDescripcion" class="materialize-textarea"></textarea>
							<label for="Descripcion">Textarea</label>
						</div>
					</div>
				</form>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<div id="EditarCurso" class="btn">Nuevo Curso</div>
				</div>
			</div>
		</form>
	</div>
</div>	